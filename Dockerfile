#---Build stage---
FROM --platform=linux/amd64 golang:1.19 AS builder
COPY . /go/src/
WORKDIR /go/src/cmd/app

RUN GOOS=linux GOARCH=amd64 CGO_ENABLED=0 go build -ldflags='-w -s' -o /go/bin/service

WORKDIR /go/src/cmd/console
RUN GOOS=linux GOARCH=amd64 CGO_ENABLED=0 go build -ldflags='-w -s' -o /go/bin/console

#---Final stage---
FROM --platform=linux/amd64 alpine:latest
COPY --from=builder /go/bin/service /go/bin/service
COPY --from=builder /go/src/migrations /migrations
COPY --from=builder /go/bin/console /go/bin/console
COPY --from=builder /usr/local/go/lib/time/zoneinfo.zip /go/bin
ENV TZ=Europe/Moscow
ENV ZONEINFO=/go/bin/zoneinfo.zip
CMD /go/bin/service --port 8090 --host '0.0.0.0'
