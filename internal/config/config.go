package config

import (
	"fmt"
	"time"

	"github.com/vrischmann/envconfig"
)

type Config struct {
	App struct {
		ServiceName string `envconfig:"default=tennis-backend"`
		Port        int    `envconfig:"default=8080"`
		Env         string `envconfig:"default=prod"`
	}

	Log struct {
		Level string `envconfig:"default=debug"`
	}

	PG struct {
		User    string `envconfig:"default=admin"`
		Pass    string `envconfig:"default=Eq4sKuRvsECP8z34eTG4VyhVrIHmbfWc"`
		Port    string `envconfig:"default=5432"`
		Host    string `envconfig:"default=localhost"`
		PoolMax int    `envconfig:"default=10"`
		DbName  string `envconfig:"default=tennis"`
		Timeout int    `envconfig:"default=1"`
	}

	Token struct {
		TTL time.Duration `envconfig:"default=120m"`
	}
}

func InitConfig(prefix string) (*Config, error) {
	conf := &Config{}
	if err := envconfig.InitWithPrefix(conf, prefix); err != nil {
		return nil, fmt.Errorf("init config error: %w", err)
	}

	return conf, nil
}
