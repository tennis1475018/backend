package group

import (
	"time"

	"github.com/google/uuid"
)

type GroupResponse struct {
	Id           uuid.UUID   `json:"id"`
	TournamentId uuid.UUID   `json:"tournament_id"`
	Name         string      `json:"name"`
	Players      []uuid.UUID `json:"players"`
	CreatedAt    time.Time   `json:"createdAt"`
	ModifiedAt   time.Time   `json:"modifiedAt"`
}
