package group

import "backend/internal/domain/group"

func ToGroupResponse(group *group.Group) *GroupResponse {
	return &GroupResponse{
		Id:           group.Id(),
		TournamentId: group.TournamentId(),
		Name:         group.Name(),
		Players:      group.Players(),
		CreatedAt:    group.CreatedAt(),
		ModifiedAt:   group.ModifiedAt(),
	}
}
