package delivery

import (
	playerRequest "backend/internal/delivery/player"
	"backend/internal/domain/player"
	"context"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
)

// CreatePlayer
// @Summary создание игрока.
// @Description создание игрока.
// @Tags players
// @Accept  json
// @Produce json
// @Param   user 	body 		playerRequest.CreatePlayerRequest 		    true  "Данные игрока"
// @Success 200			{object}    playerRequest.PlayerResponse
// @Failure 400 		{object}    ErrorResponse
// @Failure 401 		{object}    ErrorResponse
// @Router /api/player [post]
func (d *Delivery) CreatePlayer(c *gin.Context) {
	request := playerRequest.CreatePlayerRequest{}

	if err := c.ShouldBind(&request); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	level, err := player.NewLevel(request.Level)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	player, err := player.NewPlayer(request.Name, request.Surname, level)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	err = d.services.Player.CreatePlayer(context.Background(), player)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, playerRequest.ToPlayerResponse(player))
}

// UpdatePlayer
// @Summary обновление игрока.
// @Description обновление игрока.
// @Tags players
// @Accept  json
// @Produce json
// @Param   id 			path 		string 						true  "Идентификатор игрока"
// @Param   user 	body 		playerRequest.UpdatePlayerRequest 		    true  "Данные для обновления игрока"
// @Success 200			{object}    playerRequest.PlayerResponse
// @Failure 400 		{object}    ErrorResponse
// @Failure 401 		{object}    ErrorResponse
// @Router /api/player/{id} [put]
func (d *Delivery) UpdatePlayer(c *gin.Context) {
	id, err := uuid.Parse(c.Param("id"))
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	request := playerRequest.UpdatePlayerRequest{}

	if err := c.ShouldBind(&request); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	level, err := player.NewLevel(request.Level)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	upFn := func(oldPlayer *player.Player) (*player.Player, error) {
		return player.NewPlayerWithId(oldPlayer.Id(), request.Name, request.Surname, level, oldPlayer.CreatedAt(), time.Now())
	}

	player, err := d.services.Player.UpdatePlayer(context.Background(), id, upFn)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, playerRequest.ToPlayerResponse(player))
}

// ReadPlayerById
// @Summary получить игрока по id.
// @Description получить игрока по id.
// @Tags players
// @Accept  json
// @Produce json
// @Param   id 			path 		string 						true  "Идентификатор игрока"
// @Success 200			{object}    playerRequest.PlayerResponse
// @Failure 400 		{object}    ErrorResponse
// @Failure 401 		{object}    ErrorResponse
// @Router /api/player/{id} [get]
func (d *Delivery) ReadPlayerById(c *gin.Context) {
	id, err := uuid.Parse(c.Param("id"))
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	player, err := d.services.Player.ReadPlayerById(context.Background(), id)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, playerRequest.ToPlayerResponse(player))
}

// DeletePlayer
// @Summary удалить игрока.
// @Description удалить игрока.
// @Tags players
// @Accept  json
// @Produce json
// @Param   id 			path 		string 						true  "Идентификатор игрока"
// @Success 200			{object}    playerRequest.PlayerResponse
// @Failure 400 		{object}    ErrorResponse
// @Failure 401 		{object}    ErrorResponse
// @Router /api/player/{id} [delete]
func (d *Delivery) DeletePlayer(c *gin.Context) {
	id, err := uuid.Parse(c.Param("id"))
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	err = d.services.Player.DeletePlayer(context.Background(), id)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	c.Status(http.StatusOK)
}

// ReadPlayers
// @Summary получить список всех игроков.
// @Description получить список всех игроков.
// @Tags players
// @Accept  json
// @Produce json
// @Success 200			{array}    playerRequest.PlayerResponse[]
// @Failure 400 		{object}    ErrorResponse
// @Failure 401 		{object}    ErrorResponse
// @Router /api/player [get]
func (d *Delivery) ReadPlayers(c *gin.Context) {
	domainPlayers, err := d.services.Player.ReadPlayers(context.Background())
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	players := make([]*playerRequest.PlayerResponse, 0, len(domainPlayers))

	for _, domainPlayer := range domainPlayers {
		players = append(players, playerRequest.ToPlayerResponse(domainPlayer))
	}

	c.JSON(http.StatusOK, players)
}
