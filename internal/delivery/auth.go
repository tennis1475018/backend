package delivery

import (
	authRequest "backend/internal/delivery/auth"
	"context"
	"net/http"

	"github.com/gin-gonic/gin"
)

// SignIn
// @Summary авторизация пользователя.
// @Description авторизация пользователя.
// @Tags auth
// @Accept  json
// @Produce json
// @Param   user 	body 		authRequest.SignInRequest 		    true  "Данные для аутентификации"
// @Success 200			{object}    authRequest.SignInResponse
// @Failure 400 		{object}    ErrorResponse
// @Failure 401 		{object}    ErrorResponse
// @Router /api/sign-in [post]
func (d *Delivery) SignIn(c *gin.Context) {
	request := authRequest.SignInRequest{}

	if err := c.ShouldBind(&request); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	signInRes, err := d.services.Auth.SignIn(context.Background(), request.Login, request.Pass)
	if err != nil {
		c.JSON(http.StatusUnauthorized, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, authRequest.SignInResponse{Token: signInRes.Token})
}
