package delivery

import (
	"context"
	"net/http"
	"time"

	playerRequest "backend/internal/delivery/player"
	tournamentRequest "backend/internal/delivery/tournament"
	"backend/internal/domain/tournament"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
)

// CreateTournament
// @Summary создание чемпионата.
// @Description создание чемпионата.
// @Tags tournaments
// @Accept  json
// @Produce json
// @Param   user 	body 		tournamentRequest.CreateTournamentRequest 		    true  "Данные игрока"
// @Success 200			{object}    tournamentRequest.TournamentResponse
// @Failure 400 		{object}    ErrorResponse
// @Failure 401 		{object}    ErrorResponse
// @Router /api/tournament [post]
func (d *Delivery) CreateTournament(c *gin.Context) {
	request := tournamentRequest.CreateTournamentRequest{}

	if err := c.ShouldBind(&request); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	start, err := time.Parse(time.RFC3339, request.StartDate)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	tournament, err := tournament.NewTournament(request.Name, start)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	err = d.services.Tournament.CreateTournament(context.Background(), tournament, request.Players)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, tournamentRequest.ToTournamentResponse(tournament))
}

// UpdateTournament
// @Summary обновление чемпионата.
// @Description обновление чемпионата.
// @Tags tournaments
// @Accept  json
// @Produce json
// @Param   id 			path 		string 						true  "Идентификатор чемпионата"
// @Param   user 	body 		tournamentRequest.UpdateTournamentRequest 		    true  "Данные для обновления игрока"
// @Success 200			{object}    tournamentRequest.TournamentResponse
// @Failure 400 		{object}    ErrorResponse
// @Failure 401 		{object}    ErrorResponse
// @Router /api/tournament/{id} [put]
func (d *Delivery) UpdateTournament(c *gin.Context) {
	id, err := uuid.Parse(c.Param("id"))
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	request := tournamentRequest.UpdateTournamentRequest{}

	if err := c.ShouldBind(&request); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	start, err := time.Parse(time.RFC3339, request.StartDate)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	upFn := func(oldTournament *tournament.Tournament) (*tournament.Tournament, error) {
		return tournament.NewTournamentWithId(oldTournament.Id(), request.Name, start, oldTournament.CreatedAt(), time.Now())
	}

	tournament, err := d.services.Tournament.UpdateTournament(context.Background(), id, upFn)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, tournamentRequest.ToTournamentResponse(tournament))
}

// ReadTournamentById
// @Summary получить чемпионат по id.
// @Description получить чемпионат по id.
// @Tags tournaments
// @Accept  json
// @Produce json
// @Param   id 			path 		string 						true  "Идентификатор игрока"
// @Success 200			{object}    tournamentRequest.TournamentResponse
// @Failure 400 		{object}    ErrorResponse
// @Failure 401 		{object}    ErrorResponse
// @Router /api/tournament/{id} [get]
func (d *Delivery) ReadTournamentById(c *gin.Context) {
	id, err := uuid.Parse(c.Param("id"))
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	tournament, err := d.services.Tournament.ReadTournamentById(context.Background(), id)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, tournamentRequest.ToTournamentResponse(tournament))
}

// DeleteTournament
// @Summary удалить чемпионат.
// @Description удалить чемпионат.
// @Tags tournaments
// @Accept  json
// @Produce json
// @Param   id 			path 		string 						true  "Идентификатор игрока"
// @Success 200			{object}    tournamentRequest.TournamentResponse
// @Failure 400 		{object}    ErrorResponse
// @Failure 401 		{object}    ErrorResponse
// @Router /api/tournament/{id} [delete]
func (d *Delivery) DeleteTournament(c *gin.Context) {
	id, err := uuid.Parse(c.Param("id"))
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	err = d.services.Tournament.DeleteTournament(context.Background(), id)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	c.Status(http.StatusOK)
}

// ReadTournaments
// @Summary получить список всех чемпионатов.
// @Description получить список всех чемпионатов.
// @Tags tournaments
// @Accept  json
// @Produce json
// @Success 200			{array}    tournamentRequest.TournamentResponse[]
// @Failure 400 		{object}    ErrorResponse
// @Failure 401 		{object}    ErrorResponse
// @Router /api/tournament [get]
func (d *Delivery) ReadTournaments(c *gin.Context) {
	domainTournaments, err := d.services.Tournament.ReadTournaments(context.Background())
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	tournaments := make([]*tournamentRequest.TournamentResponse, 0, len(domainTournaments))

	for _, domainTournament := range domainTournaments {
		tournaments = append(tournaments, tournamentRequest.ToTournamentResponse(domainTournament))
	}

	c.JSON(http.StatusOK, tournaments)
}

// AddPlayerToTournament
// @Summary добавление игрока в турнир по ходу проведения.
// @Description добавление игрока в турнир по ходу проведения.
// @Tags tournaments
// @Accept  json
// @Produce json
// @Param   id 			path 		string 						true  "Идентификатор турнира"
// @Param   user 	body 		tournamentRequest.AddPlayerToTournamentRequest 		    true  "Данные игрока"
// @Success 200			{object}    tournamentRequest.TournamentResponse
// @Failure 400 		{object}    ErrorResponse
// @Failure 401 		{object}    ErrorResponse
// @Router /api/tournament/add-player/{id} [put]
func (d *Delivery) AddPlayerToTournament(c *gin.Context) {
	id, err := uuid.Parse(c.Param("id"))
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	request := tournamentRequest.AddPlayerToTournamentRequest{}

	if err := c.ShouldBind(&request); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	err = d.services.Tournament.AddPlayerToTournament(context.Background(), id, request.PlayerId)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	c.Status(http.StatusOK)
}

// ReadNotUsedPlayersOfTournament
// @Summary список игроков не участвующих в чемпионате.
// @Description список игроков не участвующих в чемпионате.
// @Tags tournaments
// @Accept  json
// @Produce json
// @Param   id 			path 		string 						true  "Идентификатор игрока"
// @Success 200			{array}    playerRequest.PlayerResponse[]
// @Failure 400 		{object}    ErrorResponse
// @Failure 401 		{object}    ErrorResponse
// @Router /api/tournament/free-players/{id} [get]
func (d *Delivery) ReadNotUsedPlayersOfTournament(c *gin.Context) {
	id, err := uuid.Parse(c.Param("id"))
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	players, err := d.services.Tournament.ReadNotUsedPlayersForTournament(context.Background(), id)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	response := make([]*playerRequest.PlayerResponse, 0, len(players))

	for _, player := range players {
		response = append(response, playerRequest.ToPlayerResponse(player))
	}

	c.JSON(http.StatusOK, response)
}
