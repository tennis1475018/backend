package tables

import (
	"github.com/google/uuid"
)

type TableResponse struct {
	TournamentId   uuid.UUID    `json:"tournament_id"`
	TournamentName string       `json:"tournament_name"`
	GroupId        uuid.UUID    `json:"group_id"`
	GroupName      string       `json:"group_name"`
	Items          []*TableItem `json:"items"`
}

type TableItem struct {
	PlayerId    uuid.UUID `json:"player_id"`
	Place       int       `json:"place"`
	Name        string    `json:"name"`
	Surname     string    `json:"surname"`
	Level       string    `json:"level"`
	PlayedGames int       `json:"playedGames"`
	Wins        int       `json:"wins"`
	Loses       int       `json:"loses"`
	Points      int       `json:"points"`
}
