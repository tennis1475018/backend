package tables

import (
	"backend/internal/service/tables"
)

func ToTableResponse(table *tables.Table) *TableResponse {
	items := []*TableItem{}
	for _, itm := range table.Items {
		items = append(items, &TableItem{
			PlayerId:    itm.PlayerId,
			Place:       itm.Place,
			Name:        itm.Name,
			Surname:     itm.Surname,
			Level:       itm.Level,
			PlayedGames: itm.PlayedGames,
			Wins:        itm.Wins,
			Loses:       itm.Loses,
			Points:      itm.Points,
		})
	}
	return &TableResponse{
		TournamentId:   table.TournamentId,
		TournamentName: table.TournamentName,
		GroupId:        table.GroupId,
		GroupName:      table.GroupName,
		Items:          items,
	}
}
