package delivery

import (
	tableRequest "backend/internal/delivery/tables"
	"context"
	"net/http"

	"github.com/gin-gonic/gin"
)

// ReadGroupsOfCurrentTournament
// @Summary получить таблицу результатов текущего турнира.
// @Description получить таблицу результатов текущего турнира.
// @Tags tables
// @Accept  json
// @Produce json
// @Success 200			{array}    tableRequest.TableResponse[]
// @Failure 400 		{object}    ErrorResponse
// @Failure 401 		{object}    ErrorResponse
// @Router /api/tables [get]
func (d *Delivery) ReadTablesOfCurrentTournament(c *gin.Context) {
	tablesUc, err := d.services.Tables.CalcTables(context.Background())
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	tables := make([]*tableRequest.TableResponse, 0, len(tablesUc))

	for _, tableUc := range tablesUc {
		tables = append(tables, tableRequest.ToTableResponse(tableUc))
	}

	c.JSON(http.StatusOK, tables)
}
