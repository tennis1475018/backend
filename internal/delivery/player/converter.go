package player

import "backend/internal/domain/player"

func ToPlayerResponse(player *player.Player) *PlayerResponse {
	return &PlayerResponse{
		Id:         player.Id(),
		Name:       player.Name(),
		Surname:    player.Surname(),
		Level:      player.Level(),
		CreatedAt:  player.CreatedAt(),
		ModifiedAt: player.ModifiedAt(),
	}
}
