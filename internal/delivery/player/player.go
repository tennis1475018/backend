package player

import (
	"backend/internal/domain/player"
	"time"

	"github.com/google/uuid"
)

type CreatePlayerRequest struct {
	Name    string `json:"name" example:"Иван"`
	Surname string `json:"surname" example:"Иванов"`
	Level   string `json:"level" example:"junior"`
}

type UpdatePlayerRequest struct {
	Name    string `json:"name" example:"Петр"`
	Surname string `json:"surname" example:"Петров"`
	Level   string `json:"level" example:"middle"`
}

type PlayerResponse struct {
	Id         uuid.UUID    `json:"id" example:"00000000-0000-0000-0000-000000000000" format:"uuid"`
	Name       string       `json:"name" example:"Петр"`
	Surname    string       `json:"surname" example:"Петров"`
	Level      player.Level `json:"level" example:"senior"`
	CreatedAt  time.Time    `json:"createdAt" example:"2023-05-06T15:04:05Z07:00"`
	ModifiedAt time.Time    `json:"modifiedAt" example:"2023-05-06T15:04:05Z07:00"`
}
