package tournament

import "backend/internal/domain/tournament"

func ToTournamentResponse(tournament *tournament.Tournament) *TournamentResponse {
	return &TournamentResponse{
		Id:         tournament.Id(),
		Name:       tournament.Name(),
		StartDate:  tournament.StartDate(),
		CreatedAt:  tournament.CreatedAt(),
		ModifiedAt: tournament.ModifiedAt(),
	}
}
