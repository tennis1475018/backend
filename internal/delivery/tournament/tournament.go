package tournament

import (
	"time"

	"github.com/google/uuid"
)

type CreateTournamentRequest struct {
	Name      string      `json:"name" example:"Открытый чемпионат по теннису"`
	StartDate string      `json:"startDate" example:"06.05.2023"`
	Players   []uuid.UUID `json:"players" example:"00000000-0000-0000-0000-000000000000,00000000-0000-0000-0000-000000000000"`
}

type UpdateTournamentRequest struct {
	Name      string      `json:"name" example:"Открытый чемпионат по теннису"`
	StartDate string      `json:"startDate" example:"06.05.2023"`
	Players   []uuid.UUID `json:"players" example:"00000000-0000-0000-0000-000000000000,00000000-0000-0000-0000-000000000000"`
}

type TournamentResponse struct {
	Id         uuid.UUID `json:"id" example:"00000000-0000-0000-0000-000000000000" format:"uuid"`
	Name       string    `json:"name" example:"Открытый чемпионат по теннису"`
	StartDate  time.Time `json:"startDate" example:"2023-05-06T15:04:05Z07:00"`
	CreatedAt  time.Time `json:"createdAt" example:"2023-05-06T15:04:05Z07:00"`
	ModifiedAt time.Time `json:"modifiedAt" example:"2023-05-06T15:04:05Z07:00"`
}

type AddPlayerToTournamentRequest struct {
	PlayerId uuid.UUID `json:"playerId" example:"00000000-0000-0000-0000-000000000000"`
}
