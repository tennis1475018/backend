package delivery

import (
	groupRequest "backend/internal/delivery/group"
	"context"
	"net/http"

	"github.com/gin-gonic/gin"
)

// ReadGroupsOfCurrentTournament
// @Summary получить список всех групп текущего турнира.
// @Description получить всех групп текущего турнира.
// @Tags groups
// @Accept  json
// @Produce json
// @Success 200			{array}    groupRequest.GroupResponse[]
// @Failure 400 		{object}    ErrorResponse
// @Failure 401 		{object}    ErrorResponse
// @Router /api/group [get]
func (d *Delivery) ReadGroupsOfCurrentTournament(c *gin.Context) {
	domainGroups, err := d.services.Group.FindAllGroupsOfLastTournament(context.Background())
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	groups := make([]*groupRequest.GroupResponse, 0, len(domainGroups))

	for _, domainGroup := range domainGroups {
		groups = append(groups, groupRequest.ToGroupResponse(domainGroup))
	}

	c.JSON(http.StatusOK, groups)
}
