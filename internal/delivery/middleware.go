package delivery

import (
	"net/http"
	"strings"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
)

func (d *Delivery) checkAuth(c *gin.Context) {
	token := c.GetHeader("Authorization")
	if token == "" || strings.Contains("Bearer ", token) {
		c.AbortWithStatusJSON(http.StatusUnauthorized, "please authorize")
		return
	}

	token = strings.Replace(token, "Bearer ", "", 1)

	result, err := d.tm.Parse(token)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusUnauthorized, "please authorize")
		return
	}
	id, _ := uuid.Parse(result.UserID)

	setAuthInfoToContext(c, id, result.Login, result.Role)

	c.Next()
}

func setAuthInfoToContext(c *gin.Context, id uuid.UUID, login, role string) {
	c.Set("userId", id)
	c.Set("userLogin", login)
	c.Set("userRole", role)
}
