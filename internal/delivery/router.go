package delivery

import (
	docs "backend/internal/delivery/swagger/docs"

	"github.com/gin-gonic/gin"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
)

// @title hr-report backend
// @version 1.0
// @description hr-report backend
// @license.name kanya384

// @contact.name API Support
// @contact.email kanya384@mail.ru

// @BasePath /
// @securityDefinitions.apikey ApiKeyAuth
// @in header
// @name Authorization
func (d *Delivery) initRouter() *gin.Engine {

	var router = gin.New()

	d.authRoutes(router.Group("/api"))
	router.GET("/api/game/", d.ReadGamesOfCurrentTournament)
	router.GET("/api/player/", d.ReadPlayers)
	router.GET("/api/tables/", d.ReadTablesOfCurrentTournament)
	router.GET("/api/group/", d.ReadGroupsOfCurrentTournament)

	router.Use(d.checkAuth)

	d.routerDocs(router.Group("/api/docs"))
	d.playerRoutes(router.Group("/api/player"))
	d.gameRoutes(router.Group("/api/game"))
	d.tournamentRoutes(router.Group("/api/tournament"))

	return router
}

func (d *Delivery) playerRoutes(router *gin.RouterGroup) {
	router.POST("/", d.CreatePlayer)
	router.PUT("/:id", d.UpdatePlayer)
	router.DELETE("/:id", d.DeletePlayer)
	router.GET("/:id", d.ReadPlayerById)
}

func (d *Delivery) tournamentRoutes(router *gin.RouterGroup) {
	router.POST("/", d.CreateTournament)
	router.PUT("/:id", d.UpdateTournament)
	router.DELETE("/:id", d.DeleteTournament)
	router.GET("/:id", d.ReadTournamentById)
	router.GET("/", d.ReadTournaments)
	router.PUT("/add-player/:id", d.AddPlayerToTournament)
	router.GET("/free-players/:id", d.ReadNotUsedPlayersOfTournament)
}

func (d *Delivery) gameRoutes(router *gin.RouterGroup) {
	router.GET("/:id", d.ReadGameById)
	router.PUT("/:id", d.SetGameScoreRequest)
}

func (d *Delivery) authRoutes(router *gin.RouterGroup) {
	router.POST("/sign-in", d.SignIn)
}

func (d *Delivery) routerDocs(router *gin.RouterGroup) {
	docs.SwaggerInfo.BasePath = "/"

	router.Any("/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
}
