package auth

type SignInRequest struct {
	Login string `json:"login" minLength:"4" example:"admin"`
	Pass  string `json:"pass" minLength:"5"  example:"password"`
}

type SignInResponse struct {
	Token        string `json:"token"`
	RefreshToken string `json:"refreshToken"`
}
