package delivery

import (
	"backend/internal/service"
	"backend/pkg/tokenManager"
	"fmt"

	"github.com/gin-gonic/gin"
	"gitlab.com/kanya384/gotools/logger"
)

type Delivery struct {
	services *service.Service
	router   *gin.Engine
	logger   logger.Interface
	tm       tokenManager.TokenManager

	options Options
}

type Options struct{}

func New(port int, services *service.Service, tm tokenManager.TokenManager, logger logger.Interface, options Options) (err error) {

	var d = &Delivery{
		services: services,
		logger:   logger,
		tm:       tm,
	}

	d.SetOptions(options)

	d.router = d.initRouter()
	err = d.run(port)
	return
}

func (d *Delivery) SetOptions(options Options) {
	if d.options != options {
		d.options = options
	}
}

func (d *Delivery) run(port int) (err error) {
	err = d.router.Run(fmt.Sprintf(":%d", port))
	return
}
