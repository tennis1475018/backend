package game

import "backend/internal/domain/game"

func ToGameResponse(game *game.Game) *GameResponse {
	sets := make([]Set, 0, len(game.Score().Sets()))
	for _, setItem := range game.Score().Sets() {
		sets = append(sets, Set{PlayerAScore: setItem.PlayerAscore(), PlayerBScore: setItem.PlayerBscore()})
	}

	return &GameResponse{
		Id:         game.Id(),
		GroupId:    game.GroupId(),
		Tour:       game.Tour(),
		PlayerA:    game.PlayerA(),
		PlayerB:    game.PlayerB(),
		GameDay:    game.GameDay(),
		Score:      Score{Sets: sets},
		Winner:     game.Winner(),
		StartDate:  game.StartDate(),
		Deadline:   game.Deadline(),
		CreatedAt:  game.CreatedAt(),
		ModifiedAt: game.ModifiedAt(),
	}
}
