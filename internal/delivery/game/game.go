package game

import (
	"time"

	"github.com/google/uuid"
)

type GameResponse struct {
	Id         uuid.UUID `json:"id"`
	GroupId    uuid.UUID `json:"groupId"`
	Tour       int       `json:"tour"`
	PlayerA    uuid.UUID `json:"playerA"`
	PlayerB    uuid.UUID `json:"playerB"`
	GameDay    time.Time `json:"gameDay"`
	Score      Score     `json:"score"`
	Winner     uuid.UUID `json:"winner"`
	StartDate  time.Time `json:"startDate"`
	Deadline   time.Time `json:"deadline"`
	CreatedAt  time.Time `json:"createdAt"`
	ModifiedAt time.Time `json:"modifiedAt"`
}

type Score struct {
	Sets []Set `json:"sets"`
}

type Set struct {
	PlayerAScore int `json:"playerAScore"`
	PlayerBScore int `json:"playerBScore"`
}

type SetGameScoreRequest struct {
	PlayedDate time.Time `json:"played_date" example:"2023-05-06T15:04:05Z07:00"`
	Score      Score     `json:"score"`
}
