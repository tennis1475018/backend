package delivery

import (
	gameRequest "backend/internal/delivery/game"
	"backend/internal/domain/game"
	"backend/pkg/types/score"
	"backend/pkg/types/set"
	"context"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
)

// ReadGamesOfCurrentTournament
// @Summary получить список всех игр текущего турнира.
// @Description получить всех игр текущего турнира.
// @Tags games
// @Accept  json
// @Produce json
// @Success 200			{array}    gameRequest.GameResponse[]
// @Failure 400 		{object}    ErrorResponse
// @Failure 401 		{object}    ErrorResponse
// @Router /api/game [get]
func (d *Delivery) ReadGamesOfCurrentTournament(c *gin.Context) {
	domainGames, err := d.services.Game.FindAllGamesOfCurrentTournament(context.Background())
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	games := make([]*gameRequest.GameResponse, 0, len(domainGames))

	for _, domainGame := range domainGames {
		games = append(games, gameRequest.ToGameResponse(domainGame))
	}

	c.JSON(http.StatusOK, games)
}

// ReadGameById
// @Summary получить гейм по id.
// @Description получить гейм по id.
// @Tags games
// @Accept  json
// @Produce json
// @Param   id 			path 		string 						true  "Идентификатор гейма"
// @Success 200			{object}    gameRequest.GameResponse
// @Failure 400 		{object}    ErrorResponse
// @Failure 401 		{object}    ErrorResponse
// @Router /api/game/{id} [get]
func (d *Delivery) ReadGameById(c *gin.Context) {
	id, err := uuid.Parse(c.Param("id"))
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	domainGame, err := d.services.Game.ReadGameById(context.Background(), id)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, gameRequest.ToGameResponse(domainGame))
}

// SetGameScoreRequest
// @Summary установить счет матча.
// @Description установить счет матча.
// @Tags games
// @Accept  json
// @Produce json
// @Param   id 			path 		string 						true  "Идентификатор гейма"
// @Param   score 	body 		gameRequest.SetGameScoreRequest 		    true  "Счет игра"
// @Success 200			{object}    gameRequest.GameResponse
// @Failure 400 		{object}    ErrorResponse
// @Failure 401 		{object}    ErrorResponse
// @Router /api/game/{id} [put]
func (d *Delivery) SetGameScoreRequest(c *gin.Context) {
	id, err := uuid.Parse(c.Param("id"))
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	request := gameRequest.SetGameScoreRequest{}
	if err := c.ShouldBind(&request); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	upFn := func(oldGame *game.Game) (*game.Game, error) {
		sets := make([]set.Set, 0, len(request.Score.Sets))
		for _, setItem := range request.Score.Sets {
			set := set.NewSet(setItem.PlayerAScore, setItem.PlayerBScore)
			sets = append(sets, set)
		}
		score := score.NewScore(sets)

		err = oldGame.SetScore(score)
		if err != nil {
			return nil, err
		}
		return game.NewGameWithId(oldGame.Id(), oldGame.GroupId(), oldGame.Tour(), oldGame.PlayerA(), oldGame.PlayerB(), request.PlayedDate, oldGame.Score(), oldGame.Winner(), oldGame.StartDate(), oldGame.Deadline(), oldGame.CreatedAt(), oldGame.ModifiedAt())
	}

	domainGame, err := d.services.Game.UpdateGame(context.Background(), id, upFn)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, gameRequest.ToGameResponse(domainGame))
}
