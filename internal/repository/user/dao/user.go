package dao

import (
	"time"

	"github.com/google/uuid"
)

type User struct {
	Id         uuid.UUID `json:"id"`
	Name       string    `json:"name"`
	Login      string    `json:"login"`
	Pass       string    `json:"pass"`
	Role       string    `json:"role"`
	CreatedAt  time.Time `json:"created_at"`
	ModifiedAt time.Time `json:"modified_at"`
}

var ColumnsUser = []string{
	"id",
	"name",
	"login",
	"pass",
	"role",
	"created_at",
	"modified_at",
}
