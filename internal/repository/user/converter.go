package repository

import (
	"backend/internal/domain/user"
	"backend/internal/domain/user/password"
	"backend/internal/repository/user/dao"
)

func (r *Repository) toDomainUser(daoUser *dao.User) (*user.User, error) {
	password := password.NewPassword(daoUser.Pass)
	return user.NewUserWithId(daoUser.Id, daoUser.Name, daoUser.Login, password, user.UserRole(daoUser.Role), daoUser.CreatedAt, daoUser.ModifiedAt)
}
