package repository

import (
	"backend/internal/domain/user"
	"backend/internal/repository/user/dao"
	"context"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v5"
)

const (
	tableName = "users"
)

func (r *Repository) CreateUser(ctx context.Context, user *user.User) (err error) {
	rawQuery := r.Builder.Insert(tableName).Columns(dao.ColumnsUser...).Values(user.Id(), user.Name(), user.Login(), user.Pass(), user.Role(), user.CreatedAt(), user.ModifiedAt())
	query, args, err := rawQuery.ToSql()
	if err != nil {
		return
	}
	_, err = r.Pool.Exec(ctx, query, args...)
	return
}

func (r *Repository) UpdateUser(ctx context.Context, id uuid.UUID, upFn func(item *user.User) (*user.User, error)) (user *user.User, err error) {
	user, err = r.oneUser(ctx, id)
	if err != nil {
		return
	}

	upUser, err := upFn(user)
	if err != nil {
		return
	}

	rawQuery := r.Builder.Update(tableName).Set("name", upUser.Name()).Set("login", upUser.Login()).Set("pass", upUser.Pass()).Set("role", upUser.Role()).Set("modified_at", user.ModifiedAt()).Where("id = ?", id)
	query, args, err := rawQuery.ToSql()
	if err != nil {
		return
	}
	_, err = r.Pool.Exec(ctx, query, args...)
	return
}

func (r *Repository) DeleteUser(ctx context.Context, id uuid.UUID) (err error) {
	rawQuery := r.Builder.Delete(tableName).Where("id = ?", id)
	query, args, err := rawQuery.ToSql()
	if err != nil {
		return
	}
	_, err = r.Pool.Exec(ctx, query, args...)
	return
}

func (r *Repository) ReadUserById(ctx context.Context, id uuid.UUID) (user *user.User, err error) {
	return r.oneUser(ctx, id)
}

func (r *Repository) ReadUserByLogin(ctx context.Context, login string) (user *user.User, err error) {
	rawQuery := r.Builder.Select(dao.ColumnsUser...).From(tableName).Where("login = ?", login)
	query, args, _ := rawQuery.ToSql()

	row, err := r.Pool.Query(ctx, query, args...)
	if err != nil {
		return nil, err
	}
	defer row.Close()

	daoUser, err := pgx.CollectOneRow(row, pgx.RowToStructByPos[dao.User])
	if err != nil {
		return
	}

	return r.toDomainUser(&daoUser)
}

func (r *Repository) oneUser(ctx context.Context, id uuid.UUID) (user *user.User, err error) {
	rawQuery := r.Builder.Select(dao.ColumnsUser...).From(tableName).Where("id = ?", id)
	query, args, _ := rawQuery.ToSql()

	row, err := r.Pool.Query(ctx, query, args...)
	if err != nil {
		return nil, err
	}
	defer row.Close()

	daoUser, err := pgx.CollectOneRow(row, pgx.RowToStructByPos[dao.User])
	if err != nil {
		return
	}

	return r.toDomainUser(&daoUser)
}
