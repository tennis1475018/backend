package dao

import (
	"time"

	"github.com/google/uuid"
)

type Playoff struct {
	Id           uuid.UUID `bd:"id"`
	Name         string    `bd:"name"`
	PlayoffLevel int       `bd:"level"`
	TournamentId uuid.UUID `bd:"tournament_id"`
	CreatedAt    time.Time `bd:"created_at"`
	ModifiedAt   time.Time `bd:"modified_at"`
}

var PlayoffColumns = []string{
	"id",
	"name",
	"level",
	"tournament_id",
	"created_at",
	"modified_at",
}
