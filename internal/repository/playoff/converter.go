package repository

import (
	"backend/internal/domain/playoff"
	"backend/internal/repository/playoff/dao"
)

func (r *Repository) toDomainPlayoff(daoPlayoff *dao.Playoff) (*playoff.Playoff, error) {
	return playoff.NewPlayOffWithId(daoPlayoff.Id, daoPlayoff.Name, playoff.PlayoffLevel(daoPlayoff.PlayoffLevel), daoPlayoff.TournamentId, daoPlayoff.CreatedAt, daoPlayoff.ModifiedAt), nil
}

func (r *Repository) toRepositoryPlayoff(domainPlayoff *playoff.Playoff) (*dao.Playoff, error) {
	return &dao.Playoff{Id: domainPlayoff.Id(), Name: domainPlayoff.Name(), PlayoffLevel: domainPlayoff.Level().ToInt(), TournamentId: domainPlayoff.TournamentId(), CreatedAt: domainPlayoff.CreatedAt(), ModifiedAt: domainPlayoff.ModifiedAt()}, nil
}
