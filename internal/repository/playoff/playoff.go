package repository

import (
	"backend/internal/domain/playoff"
	"backend/internal/repository/playoff/dao"
	"context"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v5"
)

const (
	tableName = "play_off"
)

func (r *Repository) CreatePlayoff(ctx context.Context, playoff *playoff.Playoff) (err error) {
	daoPlayoff, err := r.toRepositoryPlayoff(playoff)
	if err != nil {
		return
	}
	rawQuery := r.Builder.Insert(tableName).Columns(dao.PlayoffColumns...).Values(daoPlayoff.Id, daoPlayoff.Name, daoPlayoff.PlayoffLevel, daoPlayoff.TournamentId, daoPlayoff.CreatedAt, daoPlayoff.ModifiedAt)
	query, args, err := rawQuery.ToSql()
	if err != nil {
		return
	}

	_, err = r.Pool.Exec(ctx, query, args...)
	return
}

func (r *Repository) UpdatePlayoff(ctx context.Context, id uuid.UUID, upFn func(item *playoff.Playoff) (*playoff.Playoff, error)) (playoff *playoff.Playoff, err error) {
	playoff, err = r.onePlayoff(ctx, id)
	if err != nil {
		return
	}

	upPlayoff, err := upFn(playoff)
	if err != nil {
		return
	}

	daoPlayoff, err := r.toRepositoryPlayoff(upPlayoff)
	if err != nil {
		return
	}

	rawQuery := r.Builder.Update(tableName).Set("name", daoPlayoff.Name).Set("level", daoPlayoff.PlayoffLevel).Set("tournament_id", daoPlayoff.TournamentId).Set("modified_at", playoff.ModifiedAt()).Where("id = ?", id)
	query, args, err := rawQuery.ToSql()
	if err != nil {
		return
	}
	_, err = r.Pool.Exec(ctx, query, args...)
	return upPlayoff, err
}

func (r *Repository) DeletePlayoff(ctx context.Context, id uuid.UUID) (err error) {
	rawQuery := r.Builder.Delete(tableName).Where("id = ?", id)
	query, args, err := rawQuery.ToSql()
	if err != nil {
		return
	}
	_, err = r.Pool.Exec(ctx, query, args...)
	return
}

func (r *Repository) ReadPlayoffById(ctx context.Context, id uuid.UUID) (playoff *playoff.Playoff, err error) {
	return r.onePlayoff(ctx, id)
}

func (r *Repository) FindAllPlayoffsOfTournament(ctx context.Context, tournamentId uuid.UUID) (playoffs []*playoff.Playoff, err error) {
	columnsWithPrefixes := []string{}
	prefix := "playoff"
	for _, column := range dao.PlayoffColumns {
		columnsWithPrefixes = append(columnsWithPrefixes, fmt.Sprintf("%s.%s", prefix, column))
	}

	rawQuery := r.Builder.Select(columnsWithPrefixes...).LeftJoin("groups as groups on groups.id = playoff.group_id").From(tableName).Where("groups.tournament_id = ?", tournamentId).OrderBy("playoff.tour")
	query, args, _ := rawQuery.ToSql()

	row, err := r.Pool.Query(ctx, query, args...)
	if err != nil {
		return nil, err
	}
	defer row.Close()

	daoPlayoffs, err := pgx.CollectRows(row, pgx.RowToStructByPos[dao.Playoff])
	if err != nil {
		return
	}

	playoffs = make([]*playoff.Playoff, 0, len(daoPlayoffs))

	for _, daoPlayoff := range daoPlayoffs {
		playoff, err := r.toDomainPlayoff(&daoPlayoff)
		if err != nil {
			return nil, err
		}
		playoffs = append(playoffs, playoff)
	}

	return
}

func (r *Repository) onePlayoff(ctx context.Context, id uuid.UUID) (playoff *playoff.Playoff, err error) {
	rawQuery := r.Builder.Select(dao.PlayoffColumns...).From(tableName).Where("id = ?", id)
	query, args, _ := rawQuery.ToSql()

	row, err := r.Pool.Query(ctx, query, args...)
	if err != nil {
		return nil, err
	}
	defer row.Close()

	daoPlayoff, err := pgx.CollectOneRow(row, pgx.RowToStructByPos[dao.Playoff])
	if err != nil {
		return
	}

	return r.toDomainPlayoff(&daoPlayoff)
}
