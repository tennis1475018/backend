package dao

import (
	"time"

	"github.com/google/uuid"
)

type Game struct {
	Id         uuid.UUID `bd:"id"`
	GroupId    uuid.UUID `bd:"group_id"`
	Tour       int       `bd:"tour"`
	PlayerA    uuid.UUID `bd:"player_a"`
	PlayerB    uuid.UUID `bd:"player_b"`
	GameDay    time.Time `bd:"game_day"`
	Score      Score     `bd:"score"`
	Winner     uuid.UUID `bd:"winner"`
	StartDate  time.Time `bd:"start_date"`
	Deadline   time.Time `bd:"deadline"`
	CreatedAt  time.Time `bd:"created_at"`
	ModifiedAt time.Time `bd:"modified_at"`
}

type Score struct {
	Sets []Set `json:"sets"`
}

type Set struct {
	PlayerAScore int `json:"playerAScore"`
	PlayerBScore int `json:"playerBScore"`
}

var GameColumns = []string{
	"id",
	"group_id",
	"tour",
	"player_a",
	"player_b",
	"game_day",
	"score",
	"winner",
	"start_date",
	"deadline",
	"created_at",
	"modified_at",
}
