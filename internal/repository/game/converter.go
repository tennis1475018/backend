package repository

import (
	"backend/internal/domain/game"
	"backend/internal/repository/game/dao"
	"backend/pkg/types/score"
	"backend/pkg/types/set"
)

func (r *Repository) toDomainGame(daoGame *dao.Game) (*game.Game, error) {
	sets := make([]set.Set, 0, len(daoGame.Score.Sets))
	for _, setItem := range daoGame.Score.Sets {
		sets = append(sets, set.NewSet(setItem.PlayerAScore, setItem.PlayerBScore))
	}
	score := score.NewScore(sets)
	return game.NewGameWithId(daoGame.Id, daoGame.GroupId, daoGame.Tour, daoGame.PlayerA, daoGame.PlayerB, daoGame.GameDay, score, daoGame.Winner, daoGame.StartDate, daoGame.Deadline, daoGame.CreatedAt, daoGame.ModifiedAt)
}

func (r *Repository) toRepositoryGame(domainGame *game.Game) (*dao.Game, error) {
	sets := make([]dao.Set, 0, len(domainGame.Score().Sets()))
	for _, setItem := range domainGame.Score().Sets() {
		sets = append(sets, dao.Set{PlayerAScore: setItem.PlayerAscore(), PlayerBScore: setItem.PlayerBscore()})
	}
	score := dao.Score{Sets: sets}
	return &dao.Game{Id: domainGame.Id(), GroupId: domainGame.GroupId(), Tour: domainGame.Tour(), PlayerA: domainGame.PlayerA(), PlayerB: domainGame.PlayerB(), GameDay: domainGame.GameDay(), Score: score, Winner: domainGame.Winner(), StartDate: domainGame.StartDate(), Deadline: domainGame.Deadline(), CreatedAt: domainGame.CreatedAt(), ModifiedAt: domainGame.ModifiedAt()}, nil
}
