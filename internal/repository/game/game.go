package repository

import (
	"backend/internal/domain/game"
	"backend/internal/repository/game/dao"
	"context"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v5"
)

const (
	tableName = "game"
)

func (r *Repository) CreateGame(ctx context.Context, game *game.Game) (err error) {
	daoGame, err := r.toRepositoryGame(game)
	if err != nil {
		return
	}
	rawQuery := r.Builder.Insert(tableName).Columns(dao.GameColumns...).Values(daoGame.Id, daoGame.GroupId, daoGame.Tour, daoGame.PlayerA, daoGame.PlayerB, daoGame.GameDay, daoGame.Score, daoGame.Winner, daoGame.StartDate, daoGame.Deadline, daoGame.CreatedAt, daoGame.ModifiedAt)
	query, args, err := rawQuery.ToSql()
	if err != nil {
		return
	}

	_, err = r.Pool.Exec(ctx, query, args...)
	return
}

func (r *Repository) UpdateGame(ctx context.Context, id uuid.UUID, upFn func(item *game.Game) (*game.Game, error)) (game *game.Game, err error) {
	game, err = r.oneGame(ctx, id)
	if err != nil {
		return
	}

	upGame, err := upFn(game)
	if err != nil {
		return
	}

	daoGame, err := r.toRepositoryGame(upGame)
	if err != nil {
		return
	}

	rawQuery := r.Builder.Update(tableName).Set("group_id", daoGame.GroupId).Set("tour", daoGame.Tour).Set("player_a", daoGame.PlayerA).Set("player_b", daoGame.PlayerB).Set("game_day", daoGame.GameDay).Set("score", daoGame.Score).Set("winner", daoGame.Winner).Set("start_date", daoGame.StartDate).Set("deadline", daoGame.Deadline).Set("modified_at", game.ModifiedAt()).Where("id = ?", id)
	query, args, err := rawQuery.ToSql()
	if err != nil {
		return
	}
	_, err = r.Pool.Exec(ctx, query, args...)
	return upGame, err
}

func (r *Repository) DeleteGame(ctx context.Context, id uuid.UUID) (err error) {
	rawQuery := r.Builder.Delete(tableName).Where("id = ?", id)
	query, args, err := rawQuery.ToSql()
	if err != nil {
		return
	}
	_, err = r.Pool.Exec(ctx, query, args...)
	return
}

func (r *Repository) ReadGameById(ctx context.Context, id uuid.UUID) (game *game.Game, err error) {
	return r.oneGame(ctx, id)
}

func (r *Repository) ReadGamesOfGroup(ctx context.Context, groupId uuid.UUID) (games []*game.Game, err error) {
	columnsWithPrefixes := []string{}
	prefix := "game"
	for _, column := range dao.GameColumns {
		columnsWithPrefixes = append(columnsWithPrefixes, fmt.Sprintf("%s.%s", prefix, column))
	}

	rawQuery := r.Builder.Select(columnsWithPrefixes...).LeftJoin("groups as groups on groups.id = game.group_id").From(tableName).Where("groups.id = ?", groupId).OrderBy("tour")
	query, args, _ := rawQuery.ToSql()

	row, err := r.Pool.Query(ctx, query, args...)
	if err != nil {
		return nil, err
	}
	defer row.Close()

	daoGames, err := pgx.CollectRows(row, pgx.RowToStructByPos[dao.Game])
	if err != nil {
		return
	}

	games = make([]*game.Game, 0, len(daoGames))

	for _, daoGame := range daoGames {
		game, err := r.toDomainGame(&daoGame)
		if err != nil {
			return nil, err
		}
		games = append(games, game)
	}

	return
}

func (r *Repository) FindAllGamesOfTournament(ctx context.Context, tournamentId uuid.UUID) (games []*game.Game, err error) {
	columnsWithPrefixes := []string{}
	prefix := "game"
	for _, column := range dao.GameColumns {
		columnsWithPrefixes = append(columnsWithPrefixes, fmt.Sprintf("%s.%s", prefix, column))
	}

	rawQuery := r.Builder.Select(columnsWithPrefixes...).LeftJoin("groups as groups on groups.id = game.group_id").From(tableName).Where("groups.tournament_id = ?", tournamentId).OrderBy("game.tour")
	query, args, _ := rawQuery.ToSql()

	row, err := r.Pool.Query(ctx, query, args...)
	if err != nil {
		return nil, err
	}
	defer row.Close()

	daoGames, err := pgx.CollectRows(row, pgx.RowToStructByPos[dao.Game])
	if err != nil {
		return
	}

	games = make([]*game.Game, 0, len(daoGames))

	for _, daoGame := range daoGames {
		game, err := r.toDomainGame(&daoGame)
		if err != nil {
			return nil, err
		}
		games = append(games, game)
	}

	return
}

func (r *Repository) oneGame(ctx context.Context, id uuid.UUID) (game *game.Game, err error) {
	rawQuery := r.Builder.Select(dao.GameColumns...).From(tableName).Where("id = ?", id)
	query, args, _ := rawQuery.ToSql()

	row, err := r.Pool.Query(ctx, query, args...)
	if err != nil {
		return nil, err
	}
	defer row.Close()

	daoGame, err := pgx.CollectOneRow(row, pgx.RowToStructByPos[dao.Game])
	if err != nil {
		return
	}

	return r.toDomainGame(&daoGame)
}
