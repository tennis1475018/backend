package repository

import (
	"backend/internal/domain/group"
	"backend/internal/repository/group/dao"
	"context"

	"github.com/Masterminds/squirrel"
	"github.com/google/uuid"
	"github.com/jackc/pgx/v5"
)

const (
	tableName = "groups"
)

func (r *Repository) CreateGroup(ctx context.Context, group *group.Group) (err error) {
	rawQuery := r.Builder.Insert(tableName).Columns(dao.GroupColumns...).Values(group.Id(), group.TournamentId(), group.Name(), group.Players(), group.CreatedAt(), group.ModifiedAt())
	query, args, err := rawQuery.ToSql()
	if err != nil {
		return
	}

	_, err = r.Pool.Exec(ctx, query, args...)
	return
}

func (r *Repository) UpdateGroup(ctx context.Context, id uuid.UUID, upFn func(item *group.Group) (*group.Group, error)) (group *group.Group, err error) {
	group, err = r.oneGroup(ctx, id)
	if err != nil {
		return
	}

	upGroup, err := upFn(group)
	if err != nil {
		return
	}

	rawQuery := r.Builder.Update(tableName).Set("tournament_id", upGroup.TournamentId()).Set("name", upGroup.Name()).Set("players", upGroup.Players()).Set("modified_at", group.ModifiedAt()).Where("id = ?", id)
	query, args, err := rawQuery.ToSql()
	if err != nil {
		return
	}
	_, err = r.Pool.Exec(ctx, query, args...)
	return
}

func (r *Repository) DeleteGroup(ctx context.Context, id uuid.UUID) (err error) {
	rawQuery := r.Builder.Delete(tableName).Where("id = ?", id)
	query, args, err := rawQuery.ToSql()
	if err != nil {
		return
	}
	_, err = r.Pool.Exec(ctx, query, args...)
	return
}

func (r *Repository) ReadGroupById(ctx context.Context, id uuid.UUID) (group *group.Group, err error) {
	return r.oneGroup(ctx, id)
}

func (r *Repository) FindAllGroupsOfTournament(ctx context.Context, tournamentId uuid.UUID) (groups []*group.Group, err error) {
	rawQuery := r.Builder.Select(dao.GroupColumns...).From(tableName).Where("tournament_id = ?", tournamentId).OrderBy("name")
	query, args, _ := rawQuery.ToSql()

	row, err := r.Pool.Query(ctx, query, args...)
	if err != nil {
		return nil, err
	}
	defer row.Close()

	daoGroups, err := pgx.CollectRows(row, pgx.RowToStructByPos[dao.Group])
	if err != nil {
		return
	}

	groups = make([]*group.Group, 0, len(daoGroups))

	for _, daoGroup := range daoGroups {
		player, err := r.toDomainGroup(&daoGroup)
		if err != nil {
			return nil, err
		}
		groups = append(groups, player)
	}
	return
}

func (r *Repository) oneGroup(ctx context.Context, id uuid.UUID) (group *group.Group, err error) {
	rawQuery := r.Builder.Select(dao.GroupColumns...).From(tableName).Where("id = ?", id)
	query, args, _ := rawQuery.ToSql()

	row, err := r.Pool.Query(ctx, query, args...)
	if err != nil {
		return nil, err
	}
	defer row.Close()

	daoGroup, err := pgx.CollectOneRow(row, pgx.RowToStructByPos[dao.Group])
	if err != nil {
		return
	}

	return r.toDomainGroup(&daoGroup)
}

func (r *Repository) ReadPlayersIdsFromGroupsOfTournament(ctx context.Context, tournamentId uuid.UUID) (playerIds []uuid.UUID, err error) {

	rawQuery := r.Builder.Select(dao.GroupColumns...).From(tableName).Where(squirrel.Eq{"groups.tournament_id": tournamentId})
	query, args, _ := rawQuery.ToSql()

	row, err := r.Pool.Query(ctx, query, args...)
	if err != nil {
		return nil, err
	}
	defer row.Close()

	daoGroups, err := pgx.CollectRows(row, pgx.RowToStructByPos[dao.Group])
	if err != nil {
		return nil, err
	}

	playerIds = make([]uuid.UUID, 0, len(daoGroups[0].Players)*len(daoGroups))

	for _, group := range daoGroups {
		playerIds = append(playerIds, group.Players...)
	}
	return
}
