package repository

import (
	"backend/internal/domain/group"
	"backend/internal/repository/group/dao"
)

func (r *Repository) toDomainGroup(daoGroup *dao.Group) (*group.Group, error) {
	return group.NewGroupWithId(daoGroup.Id, daoGroup.TournamentId, daoGroup.Name, daoGroup.Players, daoGroup.CreatedAt, daoGroup.ModifiedAt)
}
