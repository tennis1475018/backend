package dao

import (
	"time"

	"github.com/google/uuid"
)

type Group struct {
	Id           uuid.UUID   `bd:"id"`
	TournamentId uuid.UUID   `bd:"tournament_id"`
	Name         string      `bd:"name"`
	Players      []uuid.UUID `bd:"players"`
	CreatedAt    time.Time   `bd:"created_at"`
	ModifiedAt   time.Time   `bd:"modified_at"`
}

var GroupColumns = []string{
	"id",
	"tournament_id",
	"name",
	"players",
	"created_at",
	"modified_at",
}
