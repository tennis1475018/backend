package repository

import (
	"backend/internal/domain/playoffGame"
	"backend/internal/repository/playoffGame/dao"
	"backend/pkg/types/score"
	"backend/pkg/types/set"
)

func (r *Repository) toDomainPlayoffGame(daoPlayoffGame *dao.PlayoffGame) (*playoffGame.PlayoffGame, error) {
	sets := make([]set.Set, 0, len(daoPlayoffGame.Score.Sets))
	for _, setItem := range daoPlayoffGame.Score.Sets {
		sets = append(sets, set.NewSet(setItem.PlayerAScore, setItem.PlayerBScore))
	}
	score := score.NewScore(sets)
	return playoffGame.NewPlayoffGameWithId(daoPlayoffGame.Id, playoffGame.Stage(daoPlayoffGame.Stage), daoPlayoffGame.PlayoffId, daoPlayoffGame.PlayerA, daoPlayoffGame.PlayerAGroupId, daoPlayoffGame.PlayerAGroupPlace, daoPlayoffGame.PlayerB, daoPlayoffGame.PlayerBGroupId, daoPlayoffGame.PlayerBGroupPlace, daoPlayoffGame.GameDay, score, daoPlayoffGame.Winner, daoPlayoffGame.StartDate, daoPlayoffGame.Deadline, daoPlayoffGame.CreatedAt, daoPlayoffGame.ModifiedAt)
}

func (r *Repository) toRepositoryPlayoffGame(domainPlayoffGame *playoffGame.PlayoffGame) (*dao.PlayoffGame, error) {
	sets := make([]dao.Set, 0, len(domainPlayoffGame.Score().Sets()))
	for _, setItem := range domainPlayoffGame.Score().Sets() {
		sets = append(sets, dao.Set{PlayerAScore: setItem.PlayerAscore(), PlayerBScore: setItem.PlayerBscore()})
	}
	score := dao.Score{Sets: sets}
	return &dao.PlayoffGame{Id: domainPlayoffGame.Id(), Stage: string(domainPlayoffGame.Stage()), PlayoffId: domainPlayoffGame.PlayoffId(), PlayerA: domainPlayoffGame.PlayerA(), PlayerAGroupId: domainPlayoffGame.PlayerAGroupId(), PlayerAGroupPlace: domainPlayoffGame.PlayerAGroupPlace(), PlayerB: domainPlayoffGame.PlayerB(), PlayerBGroupId: domainPlayoffGame.PlayerBGroupId(), PlayerBGroupPlace: domainPlayoffGame.PlayerBGroupPlace(), GameDay: domainPlayoffGame.GameDay(), Score: score, Winner: domainPlayoffGame.Winner(), StartDate: domainPlayoffGame.StartDate(), Deadline: domainPlayoffGame.Deadline(), CreatedAt: domainPlayoffGame.CreatedAt(), ModifiedAt: domainPlayoffGame.ModifiedAt()}, nil
}
