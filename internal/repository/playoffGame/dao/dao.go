package dao

import (
	"time"

	"github.com/google/uuid"
)

type PlayoffGame struct {
	Id                uuid.UUID `bd:"id"`
	Stage             string    `bd:"stage"`
	PlayoffId         uuid.UUID `bd:"play_off_id"`
	PlayerA           uuid.UUID `bd:"player_a"`
	PlayerAGroupId    uuid.UUID `bd:"player_a_group_id"`
	PlayerAGroupPlace int       `bd:"player_a_group_place"`
	PlayerB           uuid.UUID `bd:"player_b"`
	PlayerBGroupId    uuid.UUID `bd:"player_b_group_id"`
	PlayerBGroupPlace int       `bd:"player_b_group_place"`
	GameDay           time.Time `bd:"game_day"`
	Score             Score     `bd:"score"`
	Winner            uuid.UUID `bd:"winner"`
	StartDate         time.Time `bd:"start_date"`
	Deadline          time.Time `bd:"deadline"`
	CreatedAt         time.Time `bd:"created_at"`
	ModifiedAt        time.Time `bd:"modified_at"`
}

type Score struct {
	Sets []Set `json:"sets"`
}

type Set struct {
	PlayerAScore int `json:"playerAScore"`
	PlayerBScore int `json:"playerBScore"`
}

var GameColumns = []string{
	"id",
	"stage",
	"play_off_id",
	"player_a",
	"player_a_group_id",
	"player_a_group_place",
	"player_b",
	"player_b_group_id",
	"player_b_group_place",
	"game_day",
	"score",
	"winner",
	"start_date",
	"deadline",
	"created_at",
	"modified_at",
}
