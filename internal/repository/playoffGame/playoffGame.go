package repository

import (
	"backend/internal/domain/playoffGame"
	"backend/internal/repository/playoffGame/dao"
	"context"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v5"
)

const (
	tableName = "play_off_game"
)

func (r *Repository) CreatePlayoffGame(ctx context.Context, game *playoffGame.PlayoffGame) (err error) {
	daoGame, err := r.toRepositoryPlayoffGame(game)
	if err != nil {
		return
	}
	rawQuery := r.Builder.Insert(tableName).Columns(dao.GameColumns...).Values(daoGame.Id, daoGame.Stage, daoGame.PlayoffId, daoGame.PlayerA, daoGame.PlayerAGroupId, daoGame.PlayerAGroupPlace, daoGame.PlayerB, daoGame.PlayerBGroupId, daoGame.PlayerBGroupPlace, daoGame.GameDay, daoGame.Score, daoGame.Winner, daoGame.StartDate, daoGame.Deadline, daoGame.CreatedAt, daoGame.ModifiedAt)
	query, args, err := rawQuery.ToSql()
	if err != nil {
		return
	}

	_, err = r.Pool.Exec(ctx, query, args...)
	return
}

func (r *Repository) UpdatePlayoffGame(ctx context.Context, id uuid.UUID, upFn func(item *playoffGame.PlayoffGame) (*playoffGame.PlayoffGame, error)) (playoffGame *playoffGame.PlayoffGame, err error) {
	playoffGame, err = r.oneGame(ctx, id)
	if err != nil {
		return
	}

	upGame, err := upFn(playoffGame)
	if err != nil {
		return
	}

	daoGame, err := r.toRepositoryPlayoffGame(upGame)
	if err != nil {
		return
	}

	rawQuery := r.Builder.Update(tableName).Set("stage", daoGame.Stage).Set("playoff_id", daoGame.PlayoffId).Set("player_a", daoGame.PlayerA).Set("player_a_group_id", daoGame.PlayerAGroupId).Set("player_a_group_place", daoGame.PlayerAGroupPlace).Set("player_b", daoGame.PlayerB).Set("player_b_group_id", daoGame.PlayerBGroupId).Set("player_b_group_place", daoGame.PlayerBGroupPlace).Set("game_day", daoGame.GameDay).Set("score", daoGame.Score).Set("winner", daoGame.Winner).Set("start_date", daoGame.StartDate).Set("deadline", daoGame.Deadline).Set("modified_at", playoffGame.ModifiedAt()).Where("id = ?", id)
	query, args, err := rawQuery.ToSql()
	if err != nil {
		return
	}
	_, err = r.Pool.Exec(ctx, query, args...)
	return upGame, err
}

func (r *Repository) DeletePlayoffGame(ctx context.Context, id uuid.UUID) (err error) {
	rawQuery := r.Builder.Delete(tableName).Where("id = ?", id)
	query, args, err := rawQuery.ToSql()
	if err != nil {
		return
	}
	_, err = r.Pool.Exec(ctx, query, args...)
	return
}

func (r *Repository) ReadPlayoffGameById(ctx context.Context, id uuid.UUID) (playoffGame *playoffGame.PlayoffGame, err error) {
	return r.oneGame(ctx, id)
}

func (r *Repository) ReadPlayoffGamesOfPlayoff(ctx context.Context, playoffId uuid.UUID) (playoffGames []*playoffGame.PlayoffGame, err error) {
	columnsWithPrefixes := []string{}
	prefix := "playoffGame"
	for _, column := range dao.GameColumns {
		columnsWithPrefixes = append(columnsWithPrefixes, fmt.Sprintf("%s.%s", prefix, column))
	}

	rawQuery := r.Builder.Select(columnsWithPrefixes...).LeftJoin("groups as groups on groups.id = playoffGame.group_id").From(tableName).Where("groups.id = ?", playoffId).OrderBy("tour")
	query, args, _ := rawQuery.ToSql()

	row, err := r.Pool.Query(ctx, query, args...)
	if err != nil {
		return nil, err
	}
	defer row.Close()

	daoGames, err := pgx.CollectRows(row, pgx.RowToStructByPos[dao.PlayoffGame])
	if err != nil {
		return
	}

	playoffGames = make([]*playoffGame.PlayoffGame, 0, len(daoGames))

	for _, daoGame := range daoGames {
		playoffGame, err := r.toDomainPlayoffGame(&daoGame)
		if err != nil {
			return nil, err
		}
		playoffGames = append(playoffGames, playoffGame)
	}

	return
}

func (r *Repository) FindAllPlayoffGamesOfTournament(ctx context.Context, tournamentId uuid.UUID) (playoffGames []*playoffGame.PlayoffGame, err error) {
	columnsWithPrefixes := []string{}
	prefix := "playoffGame"
	for _, column := range dao.GameColumns {
		columnsWithPrefixes = append(columnsWithPrefixes, fmt.Sprintf("%s.%s", prefix, column))
	}

	rawQuery := r.Builder.Select(columnsWithPrefixes...).LeftJoin("groups as groups on groups.id = playoffGame.group_id").From(tableName).Where("groups.tournament_id = ?", tournamentId).OrderBy("playoffGame.tour")
	query, args, _ := rawQuery.ToSql()

	row, err := r.Pool.Query(ctx, query, args...)
	if err != nil {
		return nil, err
	}
	defer row.Close()

	daoGames, err := pgx.CollectRows(row, pgx.RowToStructByPos[dao.PlayoffGame])
	if err != nil {
		return
	}

	playoffGames = make([]*playoffGame.PlayoffGame, 0, len(daoGames))

	for _, daoGame := range daoGames {
		playoffGame, err := r.toDomainPlayoffGame(&daoGame)
		if err != nil {
			return nil, err
		}
		playoffGames = append(playoffGames, playoffGame)
	}

	return
}

func (r *Repository) oneGame(ctx context.Context, id uuid.UUID) (playoffGame *playoffGame.PlayoffGame, err error) {
	rawQuery := r.Builder.Select(dao.GameColumns...).From(tableName).Where("id = ?", id)
	query, args, _ := rawQuery.ToSql()

	row, err := r.Pool.Query(ctx, query, args...)
	if err != nil {
		return nil, err
	}
	defer row.Close()

	daoGame, err := pgx.CollectOneRow(row, pgx.RowToStructByPos[dao.PlayoffGame])
	if err != nil {
		return
	}

	return r.toDomainPlayoffGame(&daoGame)
}
