package dao

import (
	"time"

	"github.com/google/uuid"
)

type Player struct {
	Id         uuid.UUID `bd:"id"`
	Name       string    `bd:"name"`
	Surname    string    `bd:"surname"`
	Level      string    `bd:"level"`
	CreatedAt  time.Time `bd:"created_at"`
	ModifiedAt time.Time `bd:"modified_at"`
}

var PlayerColumns = []string{
	"id",
	"name",
	"surname",
	"level",
	"created_at",
	"modified_at",
}
