package repository

import (
	"backend/internal/domain/player"
	"backend/internal/repository/player/dao"
)

func (r *Repository) toDomainPlayer(daoPlayer *dao.Player) (*player.Player, error) {
	return player.NewPlayerWithId(daoPlayer.Id, daoPlayer.Name, daoPlayer.Surname, player.Level(daoPlayer.Level), daoPlayer.CreatedAt, daoPlayer.ModifiedAt)
}
