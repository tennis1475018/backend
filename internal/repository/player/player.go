package repository

import (
	"backend/internal/domain/player"
	"backend/internal/repository/player/dao"
	"context"

	"github.com/Masterminds/squirrel"
	"github.com/google/uuid"
	"github.com/jackc/pgx/v5"
)

const (
	tableName = "player"
)

func (r *Repository) CreatePlayer(ctx context.Context, player *player.Player) (err error) {
	rawQuery := r.Builder.Insert(tableName).Columns(dao.PlayerColumns...).Values(player.Id(), player.Name(), player.Surname(), player.Level(), player.CreatedAt(), player.ModifiedAt())
	query, args, err := rawQuery.ToSql()
	if err != nil {
		return
	}

	_, err = r.Pool.Exec(ctx, query, args...)
	return
}

func (r *Repository) UpdatePlayer(ctx context.Context, id uuid.UUID, upFn func(item *player.Player) (*player.Player, error)) (player *player.Player, err error) {
	player, err = r.onePlayer(ctx, id)
	if err != nil {
		return
	}

	upPlayer, err := upFn(player)
	if err != nil {
		return
	}

	rawQuery := r.Builder.Update(tableName).Set("name", upPlayer.Name()).Set("surname", upPlayer.Surname()).Set("level", upPlayer.Level()).Set("modified_at", player.ModifiedAt()).Where("id = ?", id)
	query, args, err := rawQuery.ToSql()
	if err != nil {
		return
	}
	_, err = r.Pool.Exec(ctx, query, args...)
	return upPlayer, err
}

func (r *Repository) DeletePlayer(ctx context.Context, id uuid.UUID) (err error) {
	rawQuery := r.Builder.Delete(tableName).Where("id = ?", id)
	query, args, err := rawQuery.ToSql()
	if err != nil {
		return
	}
	_, err = r.Pool.Exec(ctx, query, args...)
	return
}

func (r *Repository) ReadPlayerById(ctx context.Context, id uuid.UUID) (player *player.Player, err error) {
	return r.onePlayer(ctx, id)
}

func (r *Repository) ReadPlayers(ctx context.Context) (players []*player.Player, err error) {
	rawQuery := r.Builder.Select(dao.PlayerColumns...).From(tableName)
	query, args, _ := rawQuery.ToSql()

	row, err := r.Pool.Query(ctx, query, args...)
	if err != nil {
		return nil, err
	}
	defer row.Close()

	daoPlayers, err := pgx.CollectRows(row, pgx.RowToStructByPos[dao.Player])
	if err != nil {
		return
	}

	players = make([]*player.Player, 0, len(daoPlayers))

	for _, daoPlayer := range daoPlayers {
		player, err := r.toDomainPlayer(&daoPlayer)
		if err != nil {
			return nil, err
		}
		players = append(players, player)
	}

	return
}

func (r *Repository) ReadPlayersByIds(ctx context.Context, ids []uuid.UUID) (players []*player.Player, err error) {
	rawQuery := r.Builder.Select(dao.PlayerColumns...).From(tableName).Where(squirrel.Eq{"id": ids}).OrderBy("level desc")
	query, args, _ := rawQuery.ToSql()

	row, err := r.Pool.Query(ctx, query, args...)
	if err != nil {
		return nil, err
	}
	defer row.Close()

	daoPlayers, err := pgx.CollectRows(row, pgx.RowToStructByPos[dao.Player])
	if err != nil {
		return
	}

	players = make([]*player.Player, 0, len(daoPlayers))

	for _, daoPlayer := range daoPlayers {
		player, err := r.toDomainPlayer(&daoPlayer)
		if err != nil {
			return nil, err
		}
		players = append(players, player)
	}

	return
}

func (r *Repository) onePlayer(ctx context.Context, id uuid.UUID) (player *player.Player, err error) {
	rawQuery := r.Builder.Select(dao.PlayerColumns...).From(tableName).Where("id = ?", id)
	query, args, _ := rawQuery.ToSql()

	row, err := r.Pool.Query(ctx, query, args...)
	if err != nil {
		return nil, err
	}
	defer row.Close()

	daoPlayer, err := pgx.CollectOneRow(row, pgx.RowToStructByPos[dao.Player])
	if err != nil {
		return
	}

	return r.toDomainPlayer(&daoPlayer)
}
