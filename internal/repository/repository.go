package repository

import (
	game "backend/internal/repository/game"
	group "backend/internal/repository/group"
	player "backend/internal/repository/player"
	playoff "backend/internal/repository/playoff"
	playoffGame "backend/internal/repository/playoffGame"
	tournament "backend/internal/repository/tournament"
	user "backend/internal/repository/user"

	"gitlab.com/kanya384/gotools/psql"
)

type Repository struct {
	User        *user.Repository
	Player      *player.Repository
	Tournament  *tournament.Repository
	Group       *group.Repository
	Game        *game.Repository
	Playoff     *playoff.Repository
	PlayoffGame *playoffGame.Repository
}

func NewRepository(pg *psql.Postgres) (*Repository, error) {
	user, err := user.New(pg, user.Options{})
	if err != nil {
		return nil, err
	}

	player, err := player.New(pg, player.Options{})
	if err != nil {
		return nil, err
	}

	tournament, err := tournament.New(pg, tournament.Options{})
	if err != nil {
		return nil, err
	}

	group, err := group.New(pg, group.Options{})
	if err != nil {
		return nil, err
	}

	game, err := game.New(pg, game.Options{})
	if err != nil {
		return nil, err
	}

	playoffGame, err := playoffGame.New(pg, playoffGame.Options{})
	if err != nil {
		return nil, err
	}
	playoff, err := playoff.New(pg, playoff.Options{})
	if err != nil {
		return nil, err
	}

	return &Repository{
		user,
		player,
		tournament,
		group,
		game,
		playoff,
		playoffGame,
	}, nil

}
