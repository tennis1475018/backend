package repository

import (
	"backend/internal/domain/tournament"
	"backend/internal/repository/tournament/dao"
	"context"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v5"
)

const (
	tableName = "tournament"
)

func (r *Repository) CreateTournament(ctx context.Context, tournament *tournament.Tournament) (err error) {
	rawQuery := r.Builder.Insert(tableName).Columns(dao.TournamentColumns...).Values(tournament.Id(), tournament.Name(), tournament.StartDate(), tournament.CreatedAt(), tournament.ModifiedAt())
	query, args, err := rawQuery.ToSql()
	if err != nil {
		return
	}

	_, err = r.Pool.Exec(ctx, query, args...)
	return
}

func (r *Repository) UpdateTournament(ctx context.Context, id uuid.UUID, upFn func(item *tournament.Tournament) (*tournament.Tournament, error)) (tournament *tournament.Tournament, err error) {
	tournament, err = r.oneTournament(ctx, id)
	if err != nil {
		return
	}

	upTournament, err := upFn(tournament)
	if err != nil {
		return
	}

	rawQuery := r.Builder.Update(tableName).Set("name", upTournament.Name()).Set("start_date", upTournament.StartDate()).Set("modified_at", tournament.ModifiedAt()).Where("id = ?", id)
	query, args, err := rawQuery.ToSql()
	if err != nil {
		return
	}
	_, err = r.Pool.Exec(ctx, query, args...)
	return upTournament, err
}

func (r *Repository) DeleteTournament(ctx context.Context, id uuid.UUID) (err error) {
	rawQuery := r.Builder.Delete(tableName).Where("id = ?", id)
	query, args, err := rawQuery.ToSql()
	if err != nil {
		return
	}
	_, err = r.Pool.Exec(ctx, query, args...)
	return
}

func (r *Repository) ReadTournamentById(ctx context.Context, id uuid.UUID) (tournament *tournament.Tournament, err error) {
	return r.oneTournament(ctx, id)
}

func (r *Repository) ReadLastTournament(ctx context.Context) (tournaments *tournament.Tournament, err error) {
	rawQuery := r.Builder.Select(dao.TournamentColumns...).From(tableName).OrderBy("created_at desc")
	query, args, _ := rawQuery.ToSql()

	row, err := r.Pool.Query(ctx, query, args...)
	if err != nil {
		return nil, err
	}
	defer row.Close()

	daoTournament, err := pgx.CollectOneRow(row, pgx.RowToStructByPos[dao.Tournament])
	if err != nil {
		return
	}

	return r.toDomainTournament(&daoTournament)
}

func (r *Repository) ReadTournaments(ctx context.Context) (tournaments []*tournament.Tournament, err error) {
	rawQuery := r.Builder.Select(dao.TournamentColumns...).From(tableName)
	query, args, _ := rawQuery.ToSql()

	row, err := r.Pool.Query(ctx, query, args...)
	if err != nil {
		return nil, err
	}
	defer row.Close()

	daoTournaments, err := pgx.CollectRows(row, pgx.RowToStructByPos[dao.Tournament])
	if err != nil {
		return
	}

	tournaments = make([]*tournament.Tournament, 0, len(daoTournaments))

	for _, daoTournament := range daoTournaments {
		player, err := r.toDomainTournament(&daoTournament)
		if err != nil {
			return nil, err
		}
		tournaments = append(tournaments, player)
	}

	return
}

func (r *Repository) oneTournament(ctx context.Context, id uuid.UUID) (tournament *tournament.Tournament, err error) {
	rawQuery := r.Builder.Select(dao.TournamentColumns...).From(tableName).Where("id = ?", id)
	query, args, _ := rawQuery.ToSql()

	row, err := r.Pool.Query(ctx, query, args...)
	if err != nil {
		return nil, err
	}
	defer row.Close()

	daoTournament, err := pgx.CollectOneRow(row, pgx.RowToStructByPos[dao.Tournament])
	if err != nil {
		return
	}

	return r.toDomainTournament(&daoTournament)
}
