package dao

import (
	"time"

	"github.com/google/uuid"
)

type Tournament struct {
	Id         uuid.UUID `bd:"id"`
	Name       string    `bd:"name"`
	StartDate  time.Time `bd:"start_date"`
	CreatedAt  time.Time `bd:"created_at"`
	ModifiedAt time.Time `bd:"modified_at"`
}

var TournamentColumns = []string{
	"id",
	"name",
	"start_date",
	"created_at",
	"modified_at",
}
