package repository

import (
	"backend/internal/domain/tournament"
	"backend/internal/repository/tournament/dao"
)

func (r *Repository) toDomainTournament(daoTournament *dao.Tournament) (*tournament.Tournament, error) {
	return tournament.NewTournamentWithId(daoTournament.Id, daoTournament.Name, daoTournament.StartDate, daoTournament.CreatedAt, daoTournament.ModifiedAt)
}
