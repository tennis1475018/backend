package auth

type SignInResponse struct {
	Token        string
	RefreshToken string
}
