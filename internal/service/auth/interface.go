package auth

import (
	"context"
)

type Service interface {
	SignIn(ctx context.Context, login string, pass string) (*SignInResponse, error)
}
