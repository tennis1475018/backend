package repository

import (
	"backend/internal/domain/user"
	"context"
)

type Repository interface {
	CreateUser(ctx context.Context, user *user.User) (err error)
	ReadUserByLogin(ctx context.Context, login string) (user *user.User, err error)
}
