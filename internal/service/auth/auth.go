package auth

import (
	"backend/pkg/tokenManager"
	"context"
	"errors"
	"fmt"
)

var (
	ErrNoSuchUser = errors.New("not found user with provided credetinals")
)

func (s *service) SignIn(ctx context.Context, login string, pass string) (*SignInResponse, error) {
	user, err := s.repository.ReadUserByLogin(ctx, login)
	if err != nil {
		return nil, fmt.Errorf("error reading user from repo: %s", err.Error())
	}

	if !user.Pass().IsEqualTo(pass) {
		return nil, ErrNoSuchUser
	}

	token, err := s.tm.NewJWT(tokenManager.AuthInfo{UserID: user.Id().String(), Login: user.Login(), Role: string(user.Role())})
	if err != nil {
		return nil, fmt.Errorf("error generating token: %s", err.Error())
	}

	return &SignInResponse{Token: token}, nil
}
