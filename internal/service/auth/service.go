package auth

import (
	usersRepo "backend/internal/service/auth/adapters/repository"
	"backend/pkg/tokenManager"
	"time"
)

const (
	tokenTTL = time.Hour * 2
)

type service struct {
	repository usersRepo.Repository
	tm         tokenManager.TokenManager
}

func New(userRepo usersRepo.Repository, tm tokenManager.TokenManager) (*service, error) {
	return &service{
		repository: userRepo,
		tm:         tm,
	}, nil
}
