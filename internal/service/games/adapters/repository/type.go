package adapters

import (
	"backend/internal/domain/game"
	"backend/internal/domain/tournament"
	"context"

	"github.com/google/uuid"
)

type GameRepository interface {
	FindAllGamesOfTournament(ctx context.Context, tournamentId uuid.UUID) ([]*game.Game, error)
	UpdateGame(ctx context.Context, id uuid.UUID, upFn func(item *game.Game) (*game.Game, error)) (game *game.Game, err error)
	ReadGameById(ctx context.Context, id uuid.UUID) (game *game.Game, err error)
}

type TournamentRepository interface {
	ReadLastTournament(ctx context.Context) (tournaments *tournament.Tournament, err error)
}
