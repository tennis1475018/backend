package games

import (
	"backend/internal/domain/game"
	"context"

	"github.com/google/uuid"
)

type Service interface {
	FindAllGamesOfCurrentTournament(ctx context.Context) (games []*game.Game, err error)
	UpdateGame(ctx context.Context, id uuid.UUID, upFn func(item *game.Game) (*game.Game, error)) (game *game.Game, err error)
	ReadGameById(ctx context.Context, id uuid.UUID) (game *game.Game, err error)
}
