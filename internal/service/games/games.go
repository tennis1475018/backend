package games

import (
	"backend/internal/domain/game"
	"context"

	"github.com/google/uuid"
)

func (s *service) FindAllGamesOfCurrentTournament(ctx context.Context) (games []*game.Game, err error) {
	tournament, err := s.tournamentRepo.ReadLastTournament(ctx)
	if err != nil {
		return
	}

	games, err = s.gameRepo.FindAllGamesOfTournament(ctx, tournament.Id())
	if err != nil {
		return
	}
	return
}

func (s *service) UpdateGame(ctx context.Context, id uuid.UUID, upFn func(item *game.Game) (*game.Game, error)) (game *game.Game, err error) {
	return s.gameRepo.UpdateGame(ctx, id, upFn)
}

func (s *service) ReadGameById(ctx context.Context, id uuid.UUID) (game *game.Game, err error) {
	return s.gameRepo.ReadGameById(ctx, id)
}
