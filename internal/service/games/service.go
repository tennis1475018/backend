package games

import (
	repository "backend/internal/service/games/adapters/repository"
)

type service struct {
	gameRepo       repository.GameRepository
	tournamentRepo repository.TournamentRepository
}

func New(gameRepo repository.GameRepository, tournamentRepo repository.TournamentRepository) (*service, error) {
	return &service{
		gameRepo:       gameRepo,
		tournamentRepo: tournamentRepo,
	}, nil
}
