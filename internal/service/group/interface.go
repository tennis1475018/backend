package group

import (
	"backend/internal/domain/group"
	"context"
)

type Service interface {
	FindAllGroupsOfLastTournament(ctx context.Context) ([]*group.Group, error)
}
