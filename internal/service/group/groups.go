package group

import (
	"backend/internal/domain/group"
	"context"
)

func (s *service) FindAllGroupsOfLastTournament(ctx context.Context) ([]*group.Group, error) {
	tournament, err := s.tournamentRepo.ReadLastTournament(ctx)
	if err != nil {
		return nil, err
	}
	return s.groupRepo.FindAllGroupsOfTournament(ctx, tournament.Id())
}
