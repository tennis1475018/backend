package adapters

import (
	"backend/internal/domain/group"
	"backend/internal/domain/tournament"
	"context"

	"github.com/google/uuid"
)

type GroupRepository interface {
	FindAllGroupsOfTournament(ctx context.Context, tournamentId uuid.UUID) ([]*group.Group, error)
}

type TournamentRepository interface {
	ReadLastTournament(ctx context.Context) (tournaments *tournament.Tournament, err error)
}
