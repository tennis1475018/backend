package group

import (
	repository "backend/internal/service/group/adapters/repository"
)

type service struct {
	groupRepo      repository.GroupRepository
	tournamentRepo repository.TournamentRepository
}

func New(groupRepo repository.GroupRepository, tournamentRepo repository.TournamentRepository) (*service, error) {
	return &service{
		groupRepo:      groupRepo,
		tournamentRepo: tournamentRepo,
	}, nil
}
