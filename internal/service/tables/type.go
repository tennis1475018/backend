package tables

import "github.com/google/uuid"

type Table struct {
	TournamentId   uuid.UUID
	TournamentName string
	GroupId        uuid.UUID
	GroupName      string
	Items          []*TableItem
}

type TableItem struct {
	PlayerId    uuid.UUID
	Place       int
	Name        string
	Surname     string
	Level       string
	PlayedGames int
	Wins        int
	Loses       int
	Points      int
}
