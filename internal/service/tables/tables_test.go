package tables

import (
	"backend/internal/domain/game"
	"backend/internal/domain/group"
	"backend/internal/domain/player"
	"backend/pkg/types/score"
	"backend/pkg/types/set"
	"context"
	"fmt"
	"strconv"
	"testing"
	"time"

	"github.com/google/uuid"
	"github.com/stretchr/testify/require"
)

func TestCalcTableForGroup(t *testing.T) {
	req := require.New(t)
	fmt.Println(req)
	players := genPlayers(context.Background(), 5)
	ids := make([]uuid.UUID, 0, 5)
	for _, player := range players {
		ids = append(ids, player.Id())
	}
	group, _ := group.NewGroup(uuid.New(), "Открытый чемпионат", ids)

	games := make([]*game.Game, 0, 10)

	//first vs second
	gameInput, _ := game.NewGame(group.Id(), 1, players[0].Id(), players[1].Id(), time.Now(), time.Now(), time.Now())
	scoreInput := score.NewScore([]set.Set{
		set.NewSet(6, 1), set.NewSet(7, 6),
	})
	gameInput.SetScore(scoreInput)
	games = append(games, gameInput)

	//third vs thourth
	gameInput, _ = game.NewGame(group.Id(), 1, players[2].Id(), players[3].Id(), time.Now(), time.Now(), time.Now())
	scoreInput = score.NewScore([]set.Set{
		set.NewSet(7, 6), set.NewSet(4, 6), set.NewSet(4, 6),
	})
	gameInput.SetScore(scoreInput)
	games = append(games, gameInput)

	//first vs third
	gameInput, _ = game.NewGame(group.Id(), 1, players[0].Id(), players[2].Id(), time.Now(), time.Now(), time.Now())
	scoreInput = score.NewScore([]set.Set{
		set.NewSet(6, 1), set.NewSet(6, 4),
	})
	gameInput.SetScore(scoreInput)
	games = append(games, gameInput)

	//first vs thourth
	gameInput, _ = game.NewGame(group.Id(), 1, players[0].Id(), players[3].Id(), time.Now(), time.Now(), time.Now())
	scoreInput = score.NewScore([]set.Set{
		set.NewSet(6, 2), set.NewSet(6, 4),
	})
	gameInput.SetScore(scoreInput)
	games = append(games, gameInput)

	//third vs fith
	gameInput, _ = game.NewGame(group.Id(), 1, players[2].Id(), players[4].Id(), time.Now(), time.Now(), time.Now())
	scoreInput = score.NewScore([]set.Set{
		set.NewSet(6, 4), set.NewSet(5, 7), set.NewSet(6, 4),
	})
	gameInput.SetScore(scoreInput)
	games = append(games, gameInput)

	//thourth vs fith
	gameInput, _ = game.NewGame(group.Id(), 1, players[3].Id(), players[4].Id(), time.Now(), time.Now(), time.Now())
	scoreInput = score.NewScore([]set.Set{
		set.NewSet(6, 4), set.NewSet(7, 5),
	})
	gameInput.SetScore(scoreInput)
	games = append(games, gameInput)

	//third vs second
	gameInput, _ = game.NewGame(group.Id(), 1, players[2].Id(), players[1].Id(), time.Now(), time.Now(), time.Now())
	scoreInput = score.NewScore([]set.Set{
		set.NewSet(0, 6), set.NewSet(7, 6), set.NewSet(7, 6),
	})
	gameInput.SetScore(scoreInput)
	games = append(games, gameInput)

	table, _ := calcTableForGroup(context.Background(), "", group, games, players)
	for _, item := range table.Items {
		fmt.Println(item.Name + " " + strconv.Itoa(item.PlayedGames) + " " + strconv.Itoa(item.Points))
	}
	return
}

func genPlayers(ctx context.Context, count int) (players []*player.Player) {
	players = make([]*player.Player, 0, count)
	for i := 0; i < count; i++ {
		player, _ := player.NewPlayer("player - "+strconv.Itoa(i+1), "player - "+strconv.Itoa(i), player.Senior)
		players = append(players, player)
	}
	return
}
