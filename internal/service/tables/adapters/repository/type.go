package adapters

import (
	"backend/internal/domain/game"
	"backend/internal/domain/group"
	"backend/internal/domain/player"
	"backend/internal/domain/tournament"
	"context"

	"github.com/google/uuid"
)

type GameRepository interface {
	FindAllGamesOfTournament(ctx context.Context, tournamentId uuid.UUID) ([]*game.Game, error)
	UpdateGame(ctx context.Context, id uuid.UUID, upFn func(item *game.Game) (*game.Game, error)) (game *game.Game, err error)
	ReadGameById(ctx context.Context, id uuid.UUID) (game *game.Game, err error)
}

type GroupRepository interface {
	FindAllGroupsOfTournament(ctx context.Context, tournamentId uuid.UUID) ([]*group.Group, error)
}

type TournamentRepository interface {
	ReadLastTournament(ctx context.Context) (tournaments *tournament.Tournament, err error)
}

type PlayersRepository interface {
	ReadPlayers(ctx context.Context) (players []*player.Player, err error)
}
