package tables

import (
	repository "backend/internal/service/tables/adapters/repository"
)

type service struct {
	gameRepo       repository.GameRepository
	playersRepo    repository.PlayersRepository
	groupRepo      repository.GroupRepository
	tournamentRepo repository.TournamentRepository
}

func New(gameRepo repository.GameRepository, tournamentRepo repository.TournamentRepository, groupRepo repository.GroupRepository, playersRepo repository.PlayersRepository) (*service, error) {
	return &service{
		gameRepo:       gameRepo,
		groupRepo:      groupRepo,
		tournamentRepo: tournamentRepo,
		playersRepo:    playersRepo,
	}, nil
}
