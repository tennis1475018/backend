package tables

import (
	"context"
)

type Service interface {
	CalcTables(ctx context.Context) (tables []*Table, err error)
}
