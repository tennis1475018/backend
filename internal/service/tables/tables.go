package tables

import (
	"backend/internal/domain/game"
	"backend/internal/domain/group"
	"backend/internal/domain/player"
	"context"
	"fmt"

	"github.com/bradfitz/slice"
	"github.com/google/uuid"
)

func (s *service) CalcTables(ctx context.Context) (tables []*Table, err error) {
	tournament, err := s.tournamentRepo.ReadLastTournament(ctx)
	if err != nil {
		return
	}

	groups, err := s.groupRepo.FindAllGroupsOfTournament(ctx, tournament.Id())
	if err != nil {
		return
	}

	games, err := s.gameRepo.FindAllGamesOfTournament(ctx, tournament.Id())
	if err != nil {
		return
	}

	players, err := s.playersRepo.ReadPlayers(ctx)
	if err != nil {
		return
	}

	tables = make([]*Table, 0, len(groups))

	for _, group := range groups {
		table, err := calcTableForGroup(ctx, tournament.Name(), group, games, players)
		if err != nil {
			return nil, err
		}
		tables = append(tables, table)
	}

	return
}

func calcTableForGroup(ctx context.Context, tournamentName string, group *group.Group, games []*game.Game, players []*player.Player) (table *Table, err error) {
	table = &Table{
		TournamentName: tournamentName,
		GroupName:      group.Name(),
		GroupId:        group.Id(),
	}

	items := make([]*TableItem, 0, len(group.Players()))

	for _, playerId := range group.Players() {
		var player *player.Player
		for _, playerItem := range players {
			if playerItem.Id() == playerId {
				player = playerItem
				break
			}
		}
		if player == nil {
			return nil, fmt.Errorf("player not founded")
		}
		item := TableItem{
			Place:    0,
			PlayerId: player.Id(),
			Name:     player.Name(),
			Surname:  player.Surname(),
			Level:    string(player.Level()),
			Wins:     0,
			Loses:    0,
			Points:   0,
		}

		for _, game := range games {
			if game.Winner() == uuid.Nil {
				continue
			}
			if game.PlayerA() == player.Id() || game.PlayerB() == player.Id() {
				if game.Winner() == player.Id() {
					item.Wins++
					item.Points += 3
				} else {
					item.Loses++
				}
				item.PlayedGames++
			}
		}

		items = append(items, &item)
	}

	slice.Sort(items[:], func(i, j int) bool {
		return items[i].Points > items[j].Points
	})

	ignoreIndex := make([]int, 0, len(items))

	for i := 0; i < len(items)-1; i++ {
		if intInSlice(i, ignoreIndex) {
			continue
		}
		equals := make([]*TableItem, 0, 5)
		//gets all equal places
		for index, itemCheck := range items[i+1:] {
			if itemCheck.Points == items[i].Points {
				equals = append(equals, itemCheck)
				ignoreIndex = append(ignoreIndex, i+1+index)
			} else {
				break
			}
		}

		if len(equals) > 0 {
			sortedItemsWithEqualPoints := make([]*TableItem, 0, len(equals)+1)
			sortedItemsWithEqualPoints = append(sortedItemsWithEqualPoints, items[i])
			sortedItemsWithEqualPoints = append(sortedItemsWithEqualPoints, equals...)

			slice.Sort(sortedItemsWithEqualPoints[:], func(i, j int) bool {
				for _, game := range games {
					if (game.PlayerA() == sortedItemsWithEqualPoints[i].PlayerId || game.PlayerB() == sortedItemsWithEqualPoints[i].PlayerId) && (game.PlayerA() == sortedItemsWithEqualPoints[j].PlayerId || game.PlayerB() == sortedItemsWithEqualPoints[j].PlayerId) {
						return sortedItemsWithEqualPoints[i].PlayerId == game.Winner()
					}
				}
				return false
			})

			for index, item := range sortedItemsWithEqualPoints {
				items[i+index] = item
			}
		}
	}

	for index, item := range items {
		item.Place = index + 1
	}

	table.Items = items
	return
}

func intInSlice(i int, slice []int) bool {
	for _, item := range slice {
		if item == i {
			return true
		}
	}

	return false
}
