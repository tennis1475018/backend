package tournament

import (
	"backend/internal/config"
	"backend/internal/repository"
	"backend/internal/service/tables"
	"context"
	"fmt"
	"testing"
	"time"

	lg "gitlab.com/kanya384/gotools/logger"
	"gitlab.com/kanya384/gotools/psql"

	"github.com/stretchr/testify/suite"
)

type TournamentTestSuite struct {
	suite.Suite
	tournamentService Service
}

func (suite *TournamentTestSuite) SetupSuite() {
	// set StartingNumber to one
	cfg, err := config.InitConfig("")
	if err != nil {
		panic(fmt.Sprintf("error initializing config %s", err))
	}
	logger, err := lg.New(cfg.Log.Level, cfg.App.ServiceName)
	if err != nil {
		panic(fmt.Sprintf("error initializing logger %s", err))
	}
	pg, err := psql.New(cfg.PG.Host, cfg.PG.Port, cfg.PG.DbName, cfg.PG.User, cfg.PG.Pass, psql.MaxPoolSize(cfg.PG.PoolMax), psql.ConnTimeout(time.Duration(cfg.PG.Timeout)*time.Second))
	if err != nil {
		logger.Fatal(fmt.Errorf("postgres connection error: %w", err))
	}

	//repository
	repository, err := repository.NewRepository(pg)
	if err != nil {
		logger.Fatal("storage initialization error: %s", err.Error())
	}

	tablesService, _ := tables.New(repository.Game, repository.Tournament, repository.Group, repository.Player)

	tournamentService, err := New(repository.Tournament, repository.Player, repository.Group, repository.Game, repository.Playoff, repository.PlayoffGame, tablesService)

	suite.tournamentService = tournamentService
}

// this function executes after all tests executed
func (suite *TournamentTestSuite) TearDownSuite() {
}

// this function executes after each test case
func (suite *TournamentTestSuite) TearDownTest() {
	fmt.Println("-- From TearDownTest")
}

func (suite *TournamentTestSuite) TestCreatePlayoffsForTournament() {
	tournaments, err := suite.tournamentService.ReadTournaments(context.Background())
	if err != nil {
		return
	}
	err = suite.tournamentService.CreatePlayoffsForTournament(context.Background(), tournaments[0])
	fmt.Println(err)
}

func TestTournametnTestSuite(t *testing.T) {
	suite.Run(t, new(TournamentTestSuite))
}
