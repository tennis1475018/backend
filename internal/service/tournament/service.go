package tournament

import (
	"backend/internal/service/tables"
	repository "backend/internal/service/tournament/adapters/repository"
)

type service struct {
	tournamentRepository  repository.TournamentRepository
	playersRepository     repository.PlayersRepository
	groupRepository       repository.GroupRepository
	gameRepository        repository.GameRepository
	playoffRepository     repository.PlayoffRepository
	playoffGameRepository repository.PlayoffGameRepository
	tablesService         tables.Service
}

func New(tournamentRepository repository.TournamentRepository, playersRepository repository.PlayersRepository, groupRepository repository.GroupRepository, gameRepository repository.GameRepository, playoffRepository repository.PlayoffRepository, playoffGameRepository repository.PlayoffGameRepository, tablesService tables.Service) (*service, error) {
	return &service{
		tournamentRepository:  tournamentRepository,
		playersRepository:     playersRepository,
		groupRepository:       groupRepository,
		gameRepository:        gameRepository,
		tablesService:         tablesService,
		playoffRepository:     playoffRepository,
		playoffGameRepository: playoffGameRepository,
	}, nil
}
