package tournament

import (
	"backend/internal/domain/game"
	"backend/internal/domain/group"
	"backend/internal/domain/player"
	"backend/internal/domain/playoff"
	"backend/internal/domain/playoffGame"
	"backend/internal/domain/tournament"
	"backend/pkg/types/score"
	"context"
	"fmt"
	"math/rand"
	"time"

	"github.com/google/uuid"
)

const (
	groupsCount = 2
)

func (s *service) CreateTournament(ctx context.Context, tournament *tournament.Tournament, playersIds []uuid.UUID) (err error) {
	err = s.tournamentRepository.CreateTournament(ctx, tournament)
	if err != nil {
		return fmt.Errorf("error creating tournament: %s", err.Error())
	}

	players, err := s.playersRepository.ReadPlayersByIds(ctx, playersIds)
	if err != nil {
		return err
	}

	if len(players) != len(playersIds) {
		s.tournamentRepository.DeleteTournament(ctx, tournament.Id())
		return fmt.Errorf("some players not founded in db")
	}

	if len(players) < 4 {
		s.tournamentRepository.DeleteTournament(ctx, tournament.Id())
		return fmt.Errorf("too few players")
	}

	groups := make([]*group.Group, 0, groupsCount)

	for i := 0; i < groupsCount; i++ {
		group, err := group.NewGroup(tournament.Id(), fmt.Sprintf("Группа %d", i+1), []uuid.UUID{})
		if err != nil {
			return err
		}

		groups = append(groups, group)
	}

	for _, player := range players {
		groupItems := pickGroupsWithLowestPlayersCount(groups)
		index := 0
		if len(groupItems) > 1 {
			index = getRandomInt(0, len(groupItems)-1)
		}
		groupItems[index].AddPlayerToGroup(player)
	}

	for _, group := range groups {
		err = s.groupRepository.CreateGroup(ctx, group)
		if err != nil {
			s.tournamentRepository.DeleteTournament(ctx, tournament.Id())
			return fmt.Errorf("error creating group1: %s", err.Error())
		}
	}

	for _, group := range groups {
		games, err := s.createGamesForGroup(ctx, group, tournament.StartDate())
		if err != nil {
			return err
		}

		for _, game := range games {
			err := s.gameRepository.CreateGame(ctx, game)
			if err != nil {
				s.tournamentRepository.DeleteTournament(ctx, tournament.Id())
				return err
			}
			fmt.Printf("%s playing with %s in tour %d at %s to %s\n", game.PlayerA().String(), game.PlayerB().String(), game.Tour(), game.StartDate().Format("02.01.2006"), game.Deadline().Format("02.01.2006"))
		}
	}

	return
}

type PlayerTableInfo struct {
	PlayerId  uuid.UUID
	GroupId   uuid.UUID
	GroupName string
	Place     int
}

func (s *service) CreatePlayoffsForTournament(ctx context.Context, tournament *tournament.Tournament) (err error) {
	tables, err := s.tablesService.CalcTables(context.Background())
	if err != nil {
		return
	}
	playersForGoldenPlayoff := make([]*PlayerTableInfo, 0, len(tables)*2)
	playersForSilverPlayoff := make([]*PlayerTableInfo, 0, len(tables)*2)
	playersForBronzePlayoff := make([]*PlayerTableInfo, 0, len(tables))

	for _, table := range tables {
		playersForGoldenPlayoff = append(playersForGoldenPlayoff, &PlayerTableInfo{
			PlayerId:  table.Items[0].PlayerId,
			GroupId:   table.GroupId,
			GroupName: table.GroupName,
			Place:     1,
		})
		playersForGoldenPlayoff = append(playersForGoldenPlayoff, &PlayerTableInfo{
			PlayerId:  table.Items[1].PlayerId,
			GroupId:   table.GroupId,
			GroupName: table.GroupName,
			Place:     2,
		})

		playersForSilverPlayoff = append(playersForSilverPlayoff, &PlayerTableInfo{
			PlayerId:  table.Items[2].PlayerId,
			GroupId:   table.GroupId,
			GroupName: table.GroupName,
			Place:     3,
		})
		playersForSilverPlayoff = append(playersForSilverPlayoff, &PlayerTableInfo{
			PlayerId:  table.Items[3].PlayerId,
			GroupId:   table.GroupId,
			GroupName: table.GroupName,
			Place:     4,
		})
		playersForBronzePlayoff = append(playersForBronzePlayoff, &PlayerTableInfo{
			PlayerId:  table.Items[4].PlayerId,
			GroupId:   table.GroupId,
			GroupName: table.GroupName,
			Place:     5,
		})
	}

	err = s.createPlayoff(context.Background(), "Золотой плей-офф", playoff.Top, tournament.Id(), playersForGoldenPlayoff)
	if err != nil {
		return
	}
	err = s.createPlayoff(context.Background(), "Серебрянный плей-офф", playoff.Top, tournament.Id(), playersForSilverPlayoff)
	if err != nil {
		return
	}
	err = s.createPlayoff(context.Background(), "Бронзовый плей-офф", playoff.Top, tournament.Id(), playersForBronzePlayoff)
	if err != nil {
		return
	}
	return
}

func (s *service) createPlayoff(ctx context.Context, playoffName string, playoffLevel playoff.PlayoffLevel, tournamentId uuid.UUID, players []*PlayerTableInfo) (err error) {
	playoff := playoff.NewPlayOff(playoffName, playoffLevel, tournamentId)
	err = s.playoffRepository.CreatePlayoff(context.Background(), playoff)
	if err != nil {
		return
	}

	if len(players) == 8 {
		for i := 0; i < len(players)/2; i++ {
			playoffGame, err := playoffGame.NewPlayoffGame(playoffGame.QuaterFinal, playoff.Id(), players[i].PlayerId, players[i].GroupId, players[i].Place, players[i+3].PlayerId, players[i+3].GroupId, players[i+3].Place, time.Now(), score.NewScore(nil), uuid.Nil, time.Now(), time.Now())
			if err != nil {
				return err
			}

			err = s.playoffGameRepository.CreatePlayoffGame(context.Background(), playoffGame)
			if err != nil {
				return err
			}
		}
	} else if len(players) == 4 {
		for i := 0; i < len(players)/2; i++ {
			playoffGame, err := playoffGame.NewPlayoffGame(playoffGame.SemiFinal, playoff.Id(), players[i*2].PlayerId, players[i*2].GroupId, players[i*2].Place, players[i*2+1].PlayerId, players[i*2+1].GroupId, players[i*2+1].Place, time.Now(), score.NewScore(nil), uuid.Nil, time.Now(), time.Now())
			if err != nil {
				return err
			}

			err = s.playoffGameRepository.CreatePlayoffGame(context.Background(), playoffGame)
			if err != nil {
				return err
			}
		}
	}
	return
}

func (s *service) AddPlayerToTournament(ctx context.Context, tournamentId, playerId uuid.UUID) (err error) {
	ids, err := s.groupRepository.ReadPlayersIdsFromGroupsOfTournament(ctx, tournamentId)
	if err != nil {
		return
	}

	if uuidInSlice(ids, playerId) {
		return fmt.Errorf("player already playing in tournament")
	}

	groups, err := s.groupRepository.FindAllGroupsOfTournament(ctx, tournamentId)
	if err != nil {
		return err
	}

	groups = pickGroupsWithLowestPlayersCount(groups)

	index := getRandomInt(0, len(groups)-1)

	player, err := s.playersRepository.ReadPlayerById(ctx, playerId)
	if err != nil {
		return
	}

	pickedGroup := groups[index]

	pickedGroup.AddPlayerToGroup(player)
	upFn := func(oldGroup *group.Group) (*group.Group, error) {
		return group.NewGroupWithId(oldGroup.Id(), oldGroup.TournamentId(), oldGroup.Name(), pickedGroup.Players(), oldGroup.CreatedAt(), time.Now())
	}

	_, err = s.groupRepository.UpdateGroup(ctx, pickedGroup.Id(), upFn)
	if err != nil {
		return
	}

	tournament, err := s.tournamentRepository.ReadTournamentById(ctx, tournamentId)
	if err != nil {
		return
	}

	games, err := s.gameRepository.ReadGamesOfGroup(ctx, pickedGroup.Id())
	if err != nil {
		return
	}

	tourId := games[len(games)-1].Tour() + 1

	gamesToAdd := []*game.Game{}

	for _, id := range pickedGroup.Players() {
		if id == player.Id() {
			continue
		}

		add := int((tourId - 1) / 2)
		startDate := tournament.StartDate().Add(time.Duration(add) * time.Hour * 24 * 7)
		var date time.Time
		game, err := game.NewGame(pickedGroup.Id(), tourId, player.Id(), id, date, startDate, startDate.Add(time.Hour*24*6))
		if err != nil {
			return fmt.Errorf("error creating game: %s", err.Error())
		}

		gamesToAdd = append(gamesToAdd, game)
		tourId++
	}

	for _, game := range gamesToAdd {
		err := s.gameRepository.CreateGame(ctx, game)
		if err != nil {
			s.tournamentRepository.DeleteTournament(ctx, tournament.Id())
			return err
		}
		fmt.Printf("%s playing with %s in tour %d at %s to %s\n", game.PlayerA().String(), game.PlayerB().String(), game.Tour(), game.StartDate().Format("02.01.2006"), game.Deadline().Format("02.01.2006"))
	}

	return
}

func (s *service) ReadNotUsedPlayersForTournament(ctx context.Context, tournamentId uuid.UUID) (players []*player.Player, err error) {
	ids, err := s.groupRepository.ReadPlayersIdsFromGroupsOfTournament(ctx, tournamentId)
	if err != nil {
		return
	}

	playersList, err := s.playersRepository.ReadPlayers(ctx)
	if err != nil {
		return
	}

	if len(playersList) == len(ids) {
		return nil, fmt.Errorf("all players added to tournament")
	}

	players = make([]*player.Player, 0, len(playersList)-len(ids))

	for _, player := range playersList {
		if uuidInSlice(ids, player.Id()) {
			continue
		}
		players = append(players, player)
	}

	return
}

func pickGroupsWithLowestPlayersCount(groups []*group.Group) (lowest []*group.Group) {
	minPlayers := 9999
	lowest = []*group.Group{}
	for _, groupItem := range groups {
		if minPlayers > len(groupItem.Players()) {
			minPlayers = len(groupItem.Players())
			lowest = []*group.Group{groupItem}
			continue
		}

		if minPlayers == len(groupItem.Players()) {
			lowest = append(lowest, groupItem)
		}
	}
	return lowest
}

func getRandomInt(min, max int) int {
	rand.Seed(time.Now().UnixNano())
	return rand.Intn(max-min+1) + min
}

func (s *service) createGamesForGroup(ctx context.Context, group *group.Group, tournamentStartDate time.Time) (games []*game.Game, err error) {
	games = []*game.Game{}
	maxTours := len(group.Players()) + 2

	for _, player := range group.Players() {
	L:
		for _, player2 := range group.Players() {
			if player == player2 {
				continue
			}
			for _, game := range games {
				if game.PlayerA() == player && game.PlayerB() == player2 || game.PlayerB() == player && game.PlayerA() == player2 {
					continue L
				}
			}

			var gameDay time.Time
			tour, err := s.getNextAvailableTourForBothPlayers(player, player2, games, maxTours)
			if err != nil {
				return nil, err
			}
			var add int
			add = (tour - 1) / 2
			startDate := tournamentStartDate.Add(time.Duration(add) * time.Hour * 24 * 7)
			game, err := game.NewGame(group.Id(), tour, player, player2, gameDay, startDate, startDate.Add(time.Hour*24*6))
			if err != nil {
				return nil, fmt.Errorf("error creating game: %s", err.Error())
			}

			games = append(games, game)
		}
	}
	return
}

func (s *service) getNextAvailableTourForBothPlayers(player1 uuid.UUID, player2 uuid.UUID, games []*game.Game, maxTours int) (int, error) {
	available1 := s.getNextAvailableToursForPlayer(player1, games, maxTours)
	available2 := s.getNextAvailableToursForPlayer(player2, games, maxTours)
	if len(available2) > len(available1) {
		for _, tour := range available2 {
			if intInSlice(available1, tour) {
				return tour, nil
			}
		}
	} else {
		for _, tour := range available1 {
			if intInSlice(available2, tour) {
				return tour, nil
			}
		}
	}
	fmt.Println(available1)
	fmt.Println(available2)
	return 0, fmt.Errorf("no available tours for %s - %s", player1, player2)
}

func (s *service) getNextAvailableToursForPlayer(playerId uuid.UUID, games []*game.Game, maxTours int) []int {
	playedTours := []int{}
	availableTours := []int{}

	for _, game := range games {
		if game.PlayerA() == playerId || game.PlayerB() == playerId {
			playedTours = append(playedTours, game.Tour())
		}
	}

	for index := 1; index <= maxTours; index++ {
		if intInSlice(playedTours, index) {
			continue
		} else {
			availableTours = append(availableTours, index)
		}
	}
	return availableTours
}

func intInSlice(slice []int, value int) bool {
	for _, item := range slice {
		if item == value {
			return true
		}
	}
	return false
}

func (s *service) UpdateTournament(ctx context.Context, id uuid.UUID, upFn func(item *tournament.Tournament) (*tournament.Tournament, error)) (tournament *tournament.Tournament, err error) {
	return s.tournamentRepository.UpdateTournament(ctx, id, upFn)
}

func (s *service) DeleteTournament(ctx context.Context, id uuid.UUID) (err error) {
	return s.tournamentRepository.DeleteTournament(ctx, id)
}

func (s *service) ReadTournamentById(ctx context.Context, id uuid.UUID) (tournament *tournament.Tournament, err error) {
	return s.tournamentRepository.ReadTournamentById(ctx, id)
}

func (s *service) ReadTournaments(ctx context.Context) (tournaments []*tournament.Tournament, err error) {
	return s.tournamentRepository.ReadTournaments(ctx)
}

func uuidInSlice(slice []uuid.UUID, search uuid.UUID) bool {
	for _, item := range slice {
		if item == search {
			return true
		}
	}
	return false
}
