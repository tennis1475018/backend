package repository

import (
	"backend/internal/domain/game"
	"backend/internal/domain/group"
	"backend/internal/domain/player"
	"backend/internal/domain/playoff"
	"backend/internal/domain/playoffGame"
	"backend/internal/domain/tournament"
	"context"

	"github.com/google/uuid"
)

type TournamentRepository interface {
	CreateTournament(ctx context.Context, tournament *tournament.Tournament) (err error)
	UpdateTournament(ctx context.Context, id uuid.UUID, upFn func(item *tournament.Tournament) (*tournament.Tournament, error)) (tournament *tournament.Tournament, err error)
	DeleteTournament(ctx context.Context, id uuid.UUID) (err error)
	ReadTournamentById(ctx context.Context, id uuid.UUID) (tournament *tournament.Tournament, err error)
	ReadTournaments(ctx context.Context) (tournaments []*tournament.Tournament, err error)
}

type PlayersRepository interface {
	ReadPlayerById(ctx context.Context, id uuid.UUID) (player *player.Player, err error)
	ReadPlayersByIds(ctx context.Context, ids []uuid.UUID) ([]*player.Player, error)
	ReadPlayers(ctx context.Context) (players []*player.Player, err error)
}

type GroupRepository interface {
	CreateGroup(ctx context.Context, group *group.Group) (err error)
	UpdateGroup(ctx context.Context, id uuid.UUID, upFn func(item *group.Group) (*group.Group, error)) (group *group.Group, err error)
	DeleteGroup(ctx context.Context, id uuid.UUID) (err error)
	ReadGroupById(ctx context.Context, id uuid.UUID) (group *group.Group, err error)
	ReadPlayersIdsFromGroupsOfTournament(ctx context.Context, tournamentId uuid.UUID) (playerIds []uuid.UUID, err error)
	FindAllGroupsOfTournament(ctx context.Context, tournamentId uuid.UUID) (groups []*group.Group, err error)
}

type GameRepository interface {
	CreateGame(ctx context.Context, group *game.Game) (err error)
	UpdateGame(ctx context.Context, id uuid.UUID, upFn func(item *game.Game) (*game.Game, error)) (game *game.Game, err error)
	DeleteGame(ctx context.Context, id uuid.UUID) (err error)
	ReadGameById(ctx context.Context, id uuid.UUID) (game *game.Game, err error)
	ReadGamesOfGroup(ctx context.Context, groupId uuid.UUID) (games []*game.Game, err error)
}

type PlayoffRepository interface {
	CreatePlayoff(ctx context.Context, playoff *playoff.Playoff) (err error)
	UpdatePlayoff(ctx context.Context, id uuid.UUID, upFn func(item *playoff.Playoff) (*playoff.Playoff, error)) (playoff *playoff.Playoff, err error)
	DeletePlayoff(ctx context.Context, id uuid.UUID) (err error)
	ReadPlayoffById(ctx context.Context, id uuid.UUID) (playoff *playoff.Playoff, err error)
	FindAllPlayoffsOfTournament(ctx context.Context, tournamentId uuid.UUID) (playoffs []*playoff.Playoff, err error)
}

type PlayoffGameRepository interface {
	CreatePlayoffGame(ctx context.Context, game *playoffGame.PlayoffGame) (err error)
	UpdatePlayoffGame(ctx context.Context, id uuid.UUID, upFn func(item *playoffGame.PlayoffGame) (*playoffGame.PlayoffGame, error)) (playoffGame *playoffGame.PlayoffGame, err error)
	DeletePlayoffGame(ctx context.Context, id uuid.UUID) (err error)
	ReadPlayoffGameById(ctx context.Context, id uuid.UUID) (playoffGame *playoffGame.PlayoffGame, err error)
	ReadPlayoffGamesOfPlayoff(ctx context.Context, playoffId uuid.UUID) (playoffGames []*playoffGame.PlayoffGame, err error)
}
