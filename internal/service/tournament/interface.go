package tournament

import (
	"backend/internal/domain/player"
	"backend/internal/domain/tournament"
	"context"

	"github.com/google/uuid"
)

type Service interface {
	CreateTournament(ctx context.Context, tournament *tournament.Tournament, players []uuid.UUID) (err error)
	UpdateTournament(ctx context.Context, id uuid.UUID, upFn func(item *tournament.Tournament) (*tournament.Tournament, error)) (tournament *tournament.Tournament, err error)
	DeleteTournament(ctx context.Context, id uuid.UUID) (err error)
	ReadTournamentById(ctx context.Context, id uuid.UUID) (tournament *tournament.Tournament, err error)
	ReadTournaments(ctx context.Context) (tournaments []*tournament.Tournament, err error)
	AddPlayerToTournament(ctx context.Context, tournamentId, playerId uuid.UUID) (err error)
	ReadNotUsedPlayersForTournament(ctx context.Context, tournamentId uuid.UUID) (players []*player.Player, err error)
	CreatePlayoffsForTournament(ctx context.Context, tournament *tournament.Tournament) (err error)
}
