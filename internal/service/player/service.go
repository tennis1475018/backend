package player

import (
	playerRepo "backend/internal/service/player/adapters/repository"
)

type service struct {
	repository playerRepo.Repository
}

func New(playerRepo playerRepo.Repository) (*service, error) {
	return &service{
		repository: playerRepo,
	}, nil
}
