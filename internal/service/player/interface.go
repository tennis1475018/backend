package player

import (
	"backend/internal/domain/player"
	"context"

	"github.com/google/uuid"
)

type Service interface {
	CreatePlayer(ctx context.Context, player *player.Player) (err error)
	UpdatePlayer(ctx context.Context, id uuid.UUID, upFn func(item *player.Player) (*player.Player, error)) (player *player.Player, err error)
	DeletePlayer(ctx context.Context, id uuid.UUID) (err error)
	ReadPlayerById(ctx context.Context, id uuid.UUID) (player *player.Player, err error)
	ReadPlayers(ctx context.Context) (players []*player.Player, err error)
}
