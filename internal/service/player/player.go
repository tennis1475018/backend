package player

import (
	"backend/internal/domain/player"
	"context"

	"github.com/google/uuid"
)

func (s *service) CreatePlayer(ctx context.Context, player *player.Player) (err error) {
	return s.repository.CreatePlayer(ctx, player)
}

func (s *service) UpdatePlayer(ctx context.Context, id uuid.UUID, upFn func(item *player.Player) (*player.Player, error)) (player *player.Player, err error) {
	return s.repository.UpdatePlayer(ctx, id, upFn)
}

func (s *service) DeletePlayer(ctx context.Context, id uuid.UUID) (err error) {
	return s.repository.DeletePlayer(ctx, id)
}

func (s *service) ReadPlayerById(ctx context.Context, id uuid.UUID) (player *player.Player, err error) {
	return s.repository.ReadPlayerById(ctx, id)
}

func (s *service) ReadPlayers(ctx context.Context) (players []*player.Player, err error) {
	return s.repository.ReadPlayers(ctx)
}
