package service

import (
	"backend/internal/repository"
	"backend/internal/service/auth"
	"backend/internal/service/games"
	"backend/internal/service/group"
	"backend/internal/service/player"
	"backend/internal/service/tables"
	"backend/internal/service/tournament"
	"backend/internal/service/user"
	"backend/pkg/tokenManager"

	"gitlab.com/kanya384/gotools/logger"
)

type Service struct {
	Auth       auth.Service
	User       user.Service
	Player     player.Service
	Tournament tournament.Service
	Game       games.Service
	Group      group.Service
	Tables     tables.Service
}

func NewServices(repository repository.Repository, tm tokenManager.TokenManager, logger logger.Interface) (*Service, error) {

	auth, err := auth.New(repository.User, tm)
	if err != nil {
		return nil, err
	}

	user, err := user.New(repository.User)
	if err != nil {
		return nil, err
	}

	player, err := player.New(repository.Player)
	if err != nil {
		return nil, err
	}

	tables, err := tables.New(repository.Game, repository.Tournament, repository.Group, repository.Player)
	if err != nil {
		return nil, err
	}

	tournament, err := tournament.New(repository.Tournament, repository.Player, repository.Group, repository.Game, repository.Playoff, repository.PlayoffGame, tables)
	if err != nil {
		return nil, err
	}
	games, err := games.New(repository.Game, repository.Tournament)
	if err != nil {
		return nil, err
	}

	group, err := group.New(repository.Group, repository.Tournament)
	if err != nil {
		return nil, err
	}

	return &Service{
		Auth:       auth,
		User:       user,
		Player:     player,
		Tournament: tournament,
		Game:       games,
		Tables:     tables,
		Group:      group,
	}, nil
}
