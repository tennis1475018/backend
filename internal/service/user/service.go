package user

import (
	usersRepo "backend/internal/service/auth/adapters/repository"
)

type service struct {
	repository usersRepo.Repository
}

func New(userRepo usersRepo.Repository) (*service, error) {
	return &service{
		repository: userRepo,
	}, nil
}
