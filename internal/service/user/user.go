package user

import (
	"backend/internal/domain/user"
	"context"
)

func (s *service) CreateUser(ctx context.Context, user *user.User) (err error) {
	return s.repository.CreateUser(ctx, user)
}
