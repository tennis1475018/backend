package repository

import (
	"context"
	"os/user"

	"github.com/google/uuid"
)

type UsersRepository interface {
	CreateUser(ctx context.Context, user *user.User) (err error)
	UpdateUser(ctx context.Context, id uuid.UUID, upFn func(item *user.User) (*user.User, error)) (user *user.User, err error)
	DeleteUser(ctx context.Context, id uuid.UUID) (err error)
	ReadUserById(ctx context.Context, id uuid.UUID) (user *user.User, err error)
}
