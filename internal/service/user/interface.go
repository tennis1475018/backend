package user

import (
	"backend/internal/domain/user"
	"context"
)

type Service interface {
	CreateUser(ctx context.Context, user *user.User) error
}
