package playoffGame

type Stage string

const (
	QuaterFinal Stage = "quater-final"
	SemiFinal   Stage = "semi-final"
	Final       Stage = "final"
)
