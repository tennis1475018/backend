package playoffGame

import (
	"backend/pkg/types/score"
	"time"

	"github.com/google/uuid"
)

type PlayoffGame struct {
	id                uuid.UUID
	stage             Stage
	playOffId         uuid.UUID
	playerA           uuid.UUID
	playerAGroupId    uuid.UUID
	playerAGroupPlace int
	playerB           uuid.UUID
	playerBGroupId    uuid.UUID
	playerBGroupPlace int
	gameDay           time.Time
	score             *score.Score
	winner            uuid.UUID
	startDate         time.Time
	deadline          time.Time
	createdAt         time.Time
	modifiedAt        time.Time
}

func NewPlayoffGameWithId(
	id uuid.UUID,
	stage Stage,
	playOffId uuid.UUID,
	playerA uuid.UUID,
	playerAGroupId uuid.UUID,
	playerAGroupPlace int,
	playerB uuid.UUID,
	playerBGroupId uuid.UUID,
	playerBGroupPlace int,
	gameDay time.Time,
	score *score.Score,
	winner uuid.UUID,
	startDate time.Time,
	deadline time.Time,
	createdAt time.Time,
	modifiedAt time.Time,
) (*PlayoffGame, error) {
	return &PlayoffGame{
		id:                id,
		stage:             stage,
		playOffId:         playOffId,
		playerA:           playerA,
		playerAGroupId:    playerAGroupId,
		playerAGroupPlace: playerAGroupPlace,
		playerB:           playerB,
		playerBGroupId:    playerBGroupId,
		playerBGroupPlace: playerBGroupPlace,
		gameDay:           gameDay,
		score:             score,
		winner:            winner,
		startDate:         startDate,
		deadline:          deadline,
		createdAt:         createdAt,
		modifiedAt:        modifiedAt,
	}, nil
}

func NewPlayoffGame(
	stage Stage,
	playOffId uuid.UUID,
	playerA uuid.UUID,
	playerAGroupId uuid.UUID,
	playerAGroupPlace int,
	playerB uuid.UUID,
	playerBGroupId uuid.UUID,
	playerBGroupPlace int,
	gameDay time.Time,
	score *score.Score,
	winner uuid.UUID,
	startDate time.Time,
	deadline time.Time,
) (*PlayoffGame, error) {
	return &PlayoffGame{
		id:                uuid.New(),
		stage:             stage,
		playOffId:         playOffId,
		playerA:           playerA,
		playerAGroupId:    playerAGroupId,
		playerAGroupPlace: playerAGroupPlace,
		playerB:           playerB,
		playerBGroupId:    playerBGroupId,
		playerBGroupPlace: playerBGroupPlace,
		gameDay:           gameDay,
		score:             score,
		winner:            winner,
		startDate:         startDate,
		deadline:          deadline,
		createdAt:         time.Now(),
		modifiedAt:        time.Now(),
	}, nil
}

func (p PlayoffGame) Id() uuid.UUID {
	return p.id
}

func (p PlayoffGame) Stage() Stage {
	return p.stage
}

func (p PlayoffGame) PlayoffId() uuid.UUID {
	return p.playOffId
}

func (p PlayoffGame) PlayerA() uuid.UUID {
	return p.playerA
}

func (p PlayoffGame) PlayerAGroupId() uuid.UUID {
	return p.playerAGroupId
}

func (p PlayoffGame) PlayerAGroupPlace() int {
	return p.playerAGroupPlace
}

func (p PlayoffGame) PlayerB() uuid.UUID {
	return p.playerB
}

func (p PlayoffGame) PlayerBGroupId() uuid.UUID {
	return p.playerBGroupId
}

func (p PlayoffGame) PlayerBGroupPlace() int {
	return p.playerBGroupPlace
}

func (p PlayoffGame) GameDay() time.Time {
	return p.gameDay
}

func (p PlayoffGame) Score() *score.Score {
	return p.score
}

func (p PlayoffGame) Winner() uuid.UUID {
	return p.winner
}

func (p PlayoffGame) StartDate() time.Time {
	return p.createdAt
}

func (p PlayoffGame) Deadline() time.Time {
	return p.modifiedAt
}

func (p PlayoffGame) CreatedAt() time.Time {
	return p.createdAt
}

func (p PlayoffGame) ModifiedAt() time.Time {
	return p.modifiedAt
}
