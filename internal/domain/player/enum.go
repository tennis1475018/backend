package player

import "fmt"

type Level string

func NewLevel(input string) (Level, error) {
	switch input {
	case string(Senior):
		return Senior, nil
	case string(Middle):
		return Middle, nil
	case string(Junior):
		return Junior, nil
	default:
		return "", fmt.Errorf("not defined player level")
	}
}

const (
	Senior Level = "senior"
	Middle Level = "middle"
	Junior Level = "junior"
)
