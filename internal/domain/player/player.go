package player

import (
	"time"

	"github.com/google/uuid"
)

type Player struct {
	id         uuid.UUID
	name       string
	surname    string
	level      Level
	createdAt  time.Time
	modifiedAt time.Time
}

func (p Player) Id() uuid.UUID {
	return p.id
}

func (p Player) Name() string {
	return p.name
}

func (p Player) Surname() string {
	return p.surname
}

func (p Player) Level() Level {
	return p.level
}

func (p Player) CreatedAt() time.Time {
	return p.createdAt
}

func (p Player) ModifiedAt() time.Time {
	return p.modifiedAt
}

func NewPlayerWithId(
	id uuid.UUID,
	name string,
	surname string,
	level Level,
	createdAt time.Time,
	modifiedAt time.Time,
) (*Player, error) {
	return &Player{
		id:         id,
		name:       name,
		surname:    surname,
		level:      level,
		createdAt:  createdAt,
		modifiedAt: modifiedAt,
	}, nil
}

func NewPlayer(
	name string,
	surname string,
	level Level,
) (*Player, error) {
	return &Player{
		id:         uuid.New(),
		name:       name,
		surname:    surname,
		level:      level,
		createdAt:  time.Now(),
		modifiedAt: time.Now(),
	}, nil
}
