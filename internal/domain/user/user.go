package user

import (
	"backend/internal/domain/user/password"
	"time"

	"github.com/google/uuid"
)

type User struct {
	id         uuid.UUID
	name       string
	login      string
	pass       password.Password
	role       UserRole
	createdAt  time.Time
	modifiedAt time.Time
}

func (u User) Id() uuid.UUID {
	return u.id
}

func (u User) Name() string {
	return u.name
}

func (u User) Login() string {
	return u.login
}

func (u User) Pass() password.Password {
	return u.pass
}

func (u User) Role() UserRole {
	return u.role
}

func (u User) CreatedAt() time.Time {
	return u.createdAt
}

func (u User) ModifiedAt() time.Time {
	return u.modifiedAt
}

func NewUser(
	name string,
	login string,
	pass password.Password,
	role UserRole,
) (*User, error) {
	return &User{
		id:         uuid.New(),
		name:       name,
		login:      login,
		pass:       pass,
		role:       role,
		createdAt:  time.Now(),
		modifiedAt: time.Now(),
	}, nil
}

func NewUserWithId(
	id uuid.UUID,
	name string,
	login string,
	pass password.Password,
	role UserRole,
	createdAt time.Time,
	modifiedAt time.Time,
) (*User, error) {
	return &User{
		id:         id,
		name:       name,
		login:      login,
		pass:       pass,
		role:       role,
		createdAt:  createdAt,
		modifiedAt: modifiedAt,
	}, nil
}
