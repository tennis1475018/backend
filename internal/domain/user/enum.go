package user

type UserRole string

func (u UserRole) String() string {
	return string(u)
}

const (
	Admin   UserRole = "admin"
	Default UserRole = "default"
)
