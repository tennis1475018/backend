package playoff

type PlayoffLevel int

func (l PlayoffLevel) ToInt() int {
	return int(l)
}

const (
	Top    PlayoffLevel = 1
	Medium PlayoffLevel = 2
	Junior PlayoffLevel = 3
)
