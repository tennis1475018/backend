package playoff

import (
	"time"

	"github.com/google/uuid"
)

type Playoff struct {
	id           uuid.UUID
	name         string
	level        PlayoffLevel
	tournamentId uuid.UUID
	createdAt    time.Time
	modifiedAt   time.Time
}

func (p Playoff) Id() uuid.UUID {
	return p.id
}

func (p Playoff) Name() string {
	return p.name
}

func (p Playoff) Level() PlayoffLevel {
	return p.level
}

func (p Playoff) TournamentId() uuid.UUID {
	return p.tournamentId
}

func (p Playoff) CreatedAt() time.Time {
	return p.createdAt
}

func (p Playoff) ModifiedAt() time.Time {
	return p.modifiedAt
}

func NewPlayOff(
	name string,
	level PlayoffLevel,
	tournamentId uuid.UUID,
) *Playoff {
	return &Playoff{
		id:           uuid.New(),
		name:         name,
		level:        level,
		tournamentId: tournamentId,
		createdAt:    time.Now(),
		modifiedAt:   time.Now(),
	}
}

func NewPlayOffWithId(
	id uuid.UUID,
	name string,
	level PlayoffLevel,
	tournamentId uuid.UUID,
	createdAt time.Time,
	modifiedAt time.Time,
) *Playoff {
	return &Playoff{
		id:           id,
		name:         name,
		level:        level,
		tournamentId: tournamentId,
		createdAt:    createdAt,
		modifiedAt:   modifiedAt,
	}
}
