package group

import (
	"backend/internal/domain/player"
	"time"

	"github.com/google/uuid"
)

type Group struct {
	id           uuid.UUID
	tournamentId uuid.UUID
	name         string
	players      []uuid.UUID
	createdAt    time.Time
	modifiedAt   time.Time
}

func (g Group) Id() uuid.UUID {
	return g.id
}

func (g Group) TournamentId() uuid.UUID {
	return g.tournamentId
}

func (g Group) Name() string {
	return g.name
}

func (g Group) Players() []uuid.UUID {
	return g.players
}

func (g *Group) AddPlayerToGroup(player *player.Player) {
	g.players = append(g.players, player.Id())
}

func (g Group) CreatedAt() time.Time {
	return g.createdAt
}

func (g Group) ModifiedAt() time.Time {
	return g.modifiedAt
}

func NewGroupWithId(
	id uuid.UUID,
	tournamentId uuid.UUID,
	name string,
	players []uuid.UUID,
	createdAt time.Time,
	modifiedAt time.Time,
) (*Group, error) {
	return &Group{
		id:           id,
		tournamentId: tournamentId,
		name:         name,
		players:      players,
		createdAt:    createdAt,
		modifiedAt:   modifiedAt,
	}, nil
}

func NewGroup(
	tournamentId uuid.UUID,
	name string,
	players []uuid.UUID,
) (*Group, error) {
	return &Group{
		id:           uuid.New(),
		tournamentId: tournamentId,
		name:         name,
		players:      players,
		createdAt:    time.Now(),
		modifiedAt:   time.Now(),
	}, nil
}
