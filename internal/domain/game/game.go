package game

import (
	"fmt"
	"time"

	"backend/pkg/types/score"

	"github.com/google/uuid"
)

type Game struct {
	id         uuid.UUID
	groupId    uuid.UUID
	tour       int
	playerA    uuid.UUID
	playerB    uuid.UUID
	gameDay    time.Time
	score      *score.Score
	winner     uuid.UUID
	startDate  time.Time
	deadline   time.Time
	createdAt  time.Time
	modifiedAt time.Time
}

func (g Game) Id() uuid.UUID {
	return g.id
}

func (g Game) GroupId() uuid.UUID {
	return g.groupId
}

func (g Game) Tour() int {
	return g.tour
}

func (g Game) PlayerA() uuid.UUID {
	return g.playerA
}

func (g Game) PlayerB() uuid.UUID {
	return g.playerB
}

func (g Game) GameDay() time.Time {
	return g.gameDay
}

func (g Game) Score() *score.Score {
	return g.score
}

func (g *Game) SetScore(score *score.Score) (err error) {
	firstWins := 0
	secondWins := 0
	for _, set := range score.Sets() {
		if set.PlayerAscore() > set.PlayerBscore() {
			firstWins++
		} else {
			secondWins++
		}
	}

	if firstWins > secondWins {
		g.winner = g.playerA
	} else if firstWins < secondWins {
		g.winner = g.playerB
	} else {
		return fmt.Errorf("score provided not correctly - no winner")
	}

	g.score = score
	return nil
}

func (g Game) Winner() uuid.UUID {
	return g.winner
}

func (g Game) StartDate() time.Time {
	return g.startDate
}

func (g Game) Deadline() time.Time {
	return g.deadline
}

func (g Game) CreatedAt() time.Time {
	return g.createdAt
}

func (g Game) ModifiedAt() time.Time {
	return g.modifiedAt
}

func NewGame(
	groupId uuid.UUID,
	tour int,
	playerA uuid.UUID,
	playerB uuid.UUID,
	gameDay time.Time,
	startDate time.Time,
	deadline time.Time,
) (*Game, error) {
	return &Game{
		id:         uuid.New(),
		groupId:    groupId,
		tour:       tour,
		playerA:    playerA,
		playerB:    playerB,
		gameDay:    gameDay,
		score:      &score.Score{},
		winner:     uuid.Nil,
		startDate:  startDate,
		deadline:   deadline,
		createdAt:  time.Now(),
		modifiedAt: time.Now(),
	}, nil
}

func NewGameWithId(
	id uuid.UUID,
	groupId uuid.UUID,
	tour int,
	playerA uuid.UUID,
	playerB uuid.UUID,
	gameDay time.Time,
	score *score.Score,
	winner uuid.UUID,
	startDate time.Time,
	deadline time.Time,
	createdAt time.Time,
	modifiedAt time.Time,
) (*Game, error) {
	return &Game{
		id:         id,
		groupId:    groupId,
		tour:       tour,
		playerA:    playerA,
		playerB:    playerB,
		score:      score,
		winner:     winner,
		gameDay:    gameDay,
		startDate:  startDate,
		deadline:   deadline,
		createdAt:  createdAt,
		modifiedAt: modifiedAt,
	}, nil
}
