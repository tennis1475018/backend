package tournament

import (
	"time"

	"github.com/google/uuid"
)

type Tournament struct {
	id         uuid.UUID
	name       string
	startDate  time.Time
	createdAt  time.Time
	modifiedAt time.Time
}

func (t Tournament) Id() uuid.UUID {
	return t.id
}

func (t Tournament) Name() string {
	return t.name
}

func (t Tournament) StartDate() time.Time {
	return t.startDate
}

func (t Tournament) CreatedAt() time.Time {
	return t.createdAt
}

func (t Tournament) ModifiedAt() time.Time {
	return t.modifiedAt
}

func NewTournament(
	name string,
	startDate time.Time,
) (*Tournament, error) {
	return &Tournament{
		id:         uuid.New(),
		name:       name,
		startDate:  startDate,
		createdAt:  time.Now(),
		modifiedAt: time.Now(),
	}, nil
}

func NewTournamentWithId(
	id uuid.UUID,
	name string,
	startDate time.Time,
	createdAt time.Time,
	modifiedAt time.Time,
) (*Tournament, error) {
	return &Tournament{
		id:         id,
		name:       name,
		startDate:  startDate,
		createdAt:  createdAt,
		modifiedAt: modifiedAt,
	}, nil
}
