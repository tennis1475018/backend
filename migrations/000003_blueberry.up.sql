CREATE TABLE chat (
    "id" uuid primary key,
    "telegram_id" integer unique not null,
    "name" varchar not null,
    "phone" varchar not null,
    "last_message" varchar not null,
    "created_at" timestamp not null,
    "modified_at" timestamp not null
);

CREATE TABLE price (
    "id" uuid primary key,
    "max_amount" integer not null,
    "one_piece_price" integer not null,
    "created_at" timestamp not null,
    "modified_at" timestamp not null
);

CREATE TABLE "order" (
    "id" uuid primary key,
    "chat_id" uuid not null,
    "address" varchar not null,
    "amount" float not null,
    "price" float not null,
    "created_at" timestamp not null,
    "modified_at" timestamp not null
);