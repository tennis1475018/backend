CREATE TABLE play_off (
    "id" uuid primary key,
    "name" varchar not null,
    "level" int not null,
    "tournament_id" uuid not null,
    "created_at" timestamp not null,
    "modified_at" timestamp not null
);

CREATE TABLE play_off_game (
    "id" uuid primary key,
    "stage" varchar not null,
    "play_off_id" uuid not null,
    "player_a" uuid not null,
    "player_a_group_id" uuid not null,
    "player_a_group_place" int not null,
    "player_b" uuid not null,
    "player_b_group_id" uuid not null,
    "player_b_group_place" int not null,
    "game_day" timestamp not null,
    "score" json not null,
    "winner" uuid not null,
    "start_date" timestamp not null,
    "deadline" timestamp not null,
    "created_at" timestamp not null,
    "modified_at" timestamp not null
);
