CREATE TABLE users (
    "id" uuid primary key,
    "name" varchar not null,
    "login" varchar unique not null,
    "pass" varchar not null,
    "role" varchar not null,
    "created_at" timestamp not null,
    "modified_at" timestamp not null
);

CREATE TABLE player (
    "id" uuid primary key,
    "name" varchar not null,
    "surname" varchar not null,
    "level" varchar not null,
    "created_at" timestamp not null,
    "modified_at" timestamp not null
);

CREATE TABLE tournament (
    "id" uuid primary key,
    "name" varchar not null,
    "start_date" timestamp not null,
    "created_at" timestamp not null,
    "modified_at" timestamp not null
);

CREATE TABLE groups (
    "id" uuid primary key,
    "tournament_id" uuid not null,
    "name" varchar not null,
    "players" uuid[] not null,
    "created_at" timestamp not null,
    "modified_at" timestamp not null
);

CREATE TABLE game (
    "id" uuid primary key,
    "group_id" uuid not null,
    "tour" int not null,
    "player_a" uuid not null,
    "player_b" uuid not null,
    "game_day" timestamp not null,
    "score" json not null,
    "winner" uuid not null,
    "start_date" timestamp not null,
    "deadline" timestamp not null,
    "created_at" timestamp not null,
    "modified_at" timestamp not null
);

ALTER TABLE "groups" ADD FOREIGN KEY ("tournament_id") REFERENCES "tournament" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE "game" ADD FOREIGN KEY ("group_id") REFERENCES "groups" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;
