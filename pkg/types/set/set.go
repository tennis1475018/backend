package set

type Set struct {
	playerAscore int
	playerBscore int
}

func (s Set) PlayerAscore() int {
	return s.playerAscore
}

func (s Set) PlayerBscore() int {
	return s.playerBscore
}

func NewSet(playerAscore, playerBscore int) Set {
	return Set{
		playerAscore: playerAscore,
		playerBscore: playerBscore,
	}
}
