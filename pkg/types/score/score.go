package score

import (
	"backend/pkg/types/set"
)

type Score struct {
	sets []set.Set
}

func (s Score) Sets() []set.Set {
	return s.sets
}

func NewScore(sets []set.Set) *Score {
	return &Score{
		sets: sets,
	}
}
