package main

import (
	"backend/internal/config"
	"backend/internal/domain/user"
	"backend/internal/domain/user/password"
	"backend/internal/repository"
	"backend/internal/service"
	"backend/pkg/tokenManager"
	"context"
	"flag"
	"fmt"
	"os"
	"time"

	lg "gitlab.com/kanya384/gotools/logger"

	"gitlab.com/kanya384/gotools/psql"
)

var (
	name  string
	login string
	pass  string
)

func main() {
	flag.StringVar(&name, "name", "", "new users name")
	flag.StringVar(&login, "login", "", "new login")
	flag.StringVar(&pass, "pass", "", "new password")
	flag.Parse()

	if name == "" {
		fmt.Print("name not provided")
		os.Exit(1)
	}

	if login == "" {
		fmt.Print("login not provided")
		os.Exit(1)
	}
	if pass == "" {
		fmt.Print("pass not provided")
		os.Exit(1)
	}

	var role user.UserRole

	role = user.Admin

	password, err := password.EncryptPassword(pass)
	if err != nil {
		panic("error generating password")
	}

	services := initApp()
	user, err := user.NewUser(name, login, password, role)
	if err != nil {
		panic("user create error")
	}

	err = services.User.CreateUser(context.Background(), user)

	if err != nil {
		fmt.Printf("user create error: %s", err.Error())
	}

	fmt.Println("user successfully created")
}

func initApp() (services *service.Service) {
	//read config
	cfg, err := config.InitConfig("")
	if err != nil {
		panic(fmt.Sprintf("error initializing config %s", err))
	}

	//setup logger
	logger, err := lg.New(cfg.Log.Level, cfg.App.ServiceName)
	if err != nil {
		panic(fmt.Sprintf("error initializing logger %s", err))
	}

	//db init
	pg, err := psql.New(cfg.PG.Host, cfg.PG.Port, cfg.PG.DbName, cfg.PG.User, cfg.PG.Pass, psql.MaxPoolSize(cfg.PG.PoolMax), psql.ConnTimeout(time.Duration(cfg.PG.Timeout)*time.Second))
	if err != nil {
		panic(fmt.Sprintf("postgres connection error: %s", err.Error()))
	}

	//repository
	repository, err := repository.NewRepository(pg)
	if err != nil {
		panic(fmt.Sprintf("storage initialization error: %s", err.Error()))
	}

	tm, err := tokenManager.NewManager(cfg.Token.TTL)
	if err != nil {
		logger.Fatal("token manager initialization error: %s", err.Error())
	}

	//services
	services, err = service.NewServices(*repository, tm, logger)
	if err != nil {
		panic(fmt.Sprintf("services initialization error: %s", err.Error()))
	}
	return
}
